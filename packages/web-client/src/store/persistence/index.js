import assert from 'assert';

export const storages = {
  local: localStorage,
  session: sessionStorage,
};
const storagesList = Object.values(storages);

export const loadState = (storage = sessionStorage) => {
  assert(storagesList.includes(storage), 'Invalid storage');
  try {
    const serializedState = storage.getItem('state');
    if (!serializedState) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

export const saveState = (storage = sessionStorage, state) => {
  assert(storagesList.includes(storage), 'Invalid storage');
  try {
    const serializedState = JSON.stringify(state);
    storage.setItem('state', serializedState);
  } catch (err) {
    console.log(`Received error ${err} when trying to saveState!`);
  }
};
