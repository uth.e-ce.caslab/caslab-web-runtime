import {
  configureStore as toolkitConfigureStore,
  getDefaultMiddleware,
} from '@reduxjs/toolkit';
import {routerMiddleware} from 'connected-react-router';
import {createBrowserHistory} from 'history';
import {throttle} from 'lodash';
import {applyMiddleware} from 'redux';
import {api} from '../utils';
import createRootReducer from './createRootReducer';
import monitorReducerEnhancer from './enhancers/monitorReducer';
import loggerMiddleware from './middleware/logger';
import {loadState, saveState, storages} from './persistence';
import {logout} from './state/auth';

export const history = createBrowserHistory();

export default function configureStore(preloadedState) {
  const sessionState = loadState(storages.session);
  const localSession = loadState(storages.local);
  const persistedState = sessionState ?? localSession ?? {};

  const middleware = [
    loggerMiddleware,
    // TODO: remove if not used later on
    routerMiddleware(history), // for dispatching history actions
    ...getDefaultMiddleware(),
  ];
  const middlewareEnhancer = applyMiddleware(...middleware);
  const enhancers = [middlewareEnhancer, monitorReducerEnhancer];
  const store = toolkitConfigureStore({
    reducer: createRootReducer(history),
    middleware,
    preloadedState: preloadedState ?? persistedState,
    enhancers,
  });
  // setup api based on store state
  const auth = store.getState().auth;
  if (auth.authenticated && auth.token) {
    api.setAuthenticationHeader(auth.token);
  }
  // add api error handler to automatically logout after unauth
  api.addInterceptor(
    'response',
    apiRes => apiRes,
    apiRes => {
      const {auth} = store.getState();
      if (
        auth.authenticated &&
        auth.token &&
        [401, 403].includes(apiRes.response.status)
      ) {
        console.log('logout', logout());
        store.dispatch(logout());
      }
      // if api response contains data error, just keep that! Otherwise return the api response
      return Promise.reject(apiRes.response.data?.error ?? apiRes);
    },
  );
  // setup store subscriber to persistently store auth state
  store.subscribe(
    throttle(() => {
      const {auth} = store.getState();
      // TODO: maybe check if auth has changed before continuing.
      let localStorageState = {};
      if (auth.rememberMe) {
        localStorageState.auth = auth;
      }
      saveState(storages.local, localStorageState);
      saveState(storages.session, {auth});
    }, 1000),
  );
  return store;
}
