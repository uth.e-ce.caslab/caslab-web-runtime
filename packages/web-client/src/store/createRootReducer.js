import {connectRouter} from 'connected-react-router';
import {combineReducers} from 'redux';
import auth from './state/auth';
import generic from './state/generic';
import alerts from './state/alerts';
import users from './state/users';
import projects from './state/projects';
import socket from './state/socket';

const createRootReducer = history =>
  combineReducers({
    router: connectRouter(history),
    generic,
    auth,
    users,
    alerts,
    projects,
    socket,
  });
export default createRootReducer;
