import {createSlice} from '@reduxjs/toolkit';
import socketIOClient from 'socket.io-client';

export const slice = createSlice({
  name: 'socket',
  initialState: {
    projectsFeed: null,
  },
  reducers: {
    privateCreate: (state, {projectsFeed}) => {
      state.projectsFeed = projectsFeed;
    },
    destroy: state => {
      if (state.projectsFeed) state.projectsFeed.disconnect();
      state.projectsFeed = null;
    },
  },
});

export const {destroy} = slice.actions;
const {privateCreate} = slice.actions;

export const create = () => (dispatch, getState) => {
  const {
    socket,
    auth: {token},
  } = getState();
  if (!token || socket.projectsFeed) return; // do nothing if missing token or already created feed
  const projectsFeed = socketIOClient('/projects', {query: `token=${token}`}); // TODO: take path from common lib
  return dispatch(
    privateCreate({
      projectsFeed,
    }),
  );
};

export default slice.reducer;
