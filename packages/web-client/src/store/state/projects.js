import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from '@reduxjs/toolkit';
import {api} from '../../utils';
import {standardReq} from '../entityUtils';

const PROJECTS_PATH = 'projects';

const projectsAdapter = createEntityAdapter({});

export const fetchProjects = createAsyncThunk(
  `${PROJECTS_PATH}/get`,
  async (unused, {getState, requestId}) => {
    const {
      projects: {loading, currentRequestId},
    } = getState();
    if (loading !== 'pending' || requestId !== currentRequestId) {
      // pending request exists so do nothing
      return;
    }
    const {data} = await api.get(PROJECTS_PATH);
    return {projects: data};
  },
);

export const createProject = createAsyncThunk(
  `${PROJECTS_PATH}/post`,
  async (data, {getState, requestId, rejectWithValue}) => {
    const {
      projects: {loading, currentRequestId},
    } = getState();
    if (loading !== 'pending' || requestId !== currentRequestId) {
      // pending request exists so do nothing
      return;
    }
    try {
      const response = await api.post(PROJECTS_PATH, data);
      return {project: response.data};
    } catch (err) {
      return rejectWithValue(err);
    }
  },
);

export const updateProject = createAsyncThunk(
  `${PROJECTS_PATH}/patch`,
  async ({id, data, reload = true}, {getState, requestId, rejectWithValue}) => {
    const {
      projects: {loading, currentRequestId},
    } = getState();
    if (loading !== 'pending' || requestId !== currentRequestId) {
      // pending request exists so do nothing
      return;
    }
    try {
      const url = `${PROJECTS_PATH}/${id}`;
      await api.patch(url, data);
      let changes;
      if (reload) {
        const res = await api.get(url);
        changes = res.data;
      } else {
        changes = data;
      }
      return {id, changes};
    } catch (err) {
      return rejectWithValue(err);
    }
  },
);

export const deleteProject = createAsyncThunk(
  `${PROJECTS_PATH}/delete`,
  async (id, {getState, requestId, rejectWithValue}) => {
    const {
      projects: {loading, currentRequestId},
    } = getState();
    if (loading !== 'pending' || requestId !== currentRequestId) {
      // pending request exists so do nothing
      return;
    }
    try {
      await api.del(`${PROJECTS_PATH}/${id}`);
      return {id};
    } catch (err) {
      return rejectWithValue(err);
    }
  },
);

export const setProjectState = createAsyncThunk(
  `${PROJECTS_PATH}/setState`,
  async (
    {id, state, reload = true},
    {getState, requestId, rejectWithValue},
  ) => {
    const {
      projects: {loading, currentRequestId},
    } = getState();
    if (loading !== 'pending' || requestId !== currentRequestId) {
      // pending request exists so do nothing
      return;
    }
    try {
      const projectUrl = `${PROJECTS_PATH}/${id}`;
      await api.post(`${projectUrl}/setState`, {state});
      let changes;
      if (reload) {
        const res = await api.get(projectUrl);
        changes = res.data;
      } else {
        changes = {state};
      }
      return {id, changes};
    } catch (err) {
      return rejectWithValue(err);
    }
  },
);

export const slice = createSlice({
  name: 'projects',
  initialState: {
    loading: 'idle',
    currentRequestId: undefined,
    ...projectsAdapter.getInitialState(),
  },
  reducers: {
    localSetProjectState: (s, {payload: {projectId, state}}) => {
      projectsAdapter.updateOne(s, {id: projectId, changes: {state}});
    },
  },
  extraReducers: {
    ...standardReq(fetchProjects, (state, payload) =>
      projectsAdapter.setAll(state, payload.projects),
    ),
    ...standardReq(createProject, (state, payload) =>
      projectsAdapter.addOne(state, payload.project),
    ),
    ...standardReq(updateProject, (state, payload) =>
      projectsAdapter.updateOne(state, payload),
    ),
    ...standardReq(deleteProject, (state, payload) =>
      projectsAdapter.removeOne(state, payload.id),
    ),
    ...standardReq(setProjectState, (state, payload) =>
      projectsAdapter.updateOne(state, payload),
    ),
  },
});

export const {localSetProjectState} = slice.actions;

export default slice.reducer;
