import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from '@reduxjs/toolkit';
import {api} from '../../utils';
import {standardReq} from '../entityUtils';

const usersAdapter = createEntityAdapter({});

export const USERS_PATH = 'users';

export const fetchUser = createAsyncThunk(
  `${USERS_PATH}/getOne`,
  async (id, {getState, requestId}) => {
    const {loading, currentRequestId} = getState().users;
    if (loading !== 'pending' || requestId !== currentRequestId) {
      // pending request exists so do nothing
      return;
    }
    const {data} = await api.get(`${USERS_PATH}/${id}`);
    return {user: data};
  },
);

export const fetchUsers = createAsyncThunk(
  `${USERS_PATH}/get`,
  async (unused, {getState, requestId}) => {
    const {loading, currentRequestId} = getState().users;
    if (loading !== 'pending' || requestId !== currentRequestId) {
      // pending request exists so do nothing
      return;
    }
    const {data} = await api.get(USERS_PATH);
    return {users: data};
  },
);

export const deleteUser = createAsyncThunk(
  `${USERS_PATH}/delete`,
  async (id, {getState, requestId, rejectWithValue}) => {
    const {loading, currentRequestId} = getState().users;
    if (loading !== 'pending' || requestId !== currentRequestId) {
      // pending request exists so do nothing
      return;
    }
    try {
      await api.del(`${USERS_PATH}/${id}`);
      return {id};
    } catch (err) {
      return rejectWithValue(err);
    }
  },
);

export const slice = createSlice({
  name: 'users',
  initialState: {
    loading: 'idle',
    currentRequestId: undefined,
    ...usersAdapter.getInitialState(),
  },
  extraReducers: {
    ...standardReq(fetchUser, (state, payload) =>
      usersAdapter.upsertOne(state, payload.user),
    ),
    ...standardReq(fetchUsers, (state, payload) =>
      usersAdapter.setAll(state, payload.users),
    ),
    ...standardReq(deleteUser, (state, payload) =>
      usersAdapter.removeOne(state, payload.id),
    ),
  },
});

export default slice.reducer;
