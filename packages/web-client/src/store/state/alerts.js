import {createSlice} from '@reduxjs/toolkit';

export const slice = createSlice({
  name: 'alerts',
  initialState: {
    isAlertHidden: true,
    alertMessage: '',
    alertStatus: 'unknown',
  },
  reducers: {
    privateShowAlert: (state, {payload}) => {
      state.isAlertHidden = false;
      state.alertMessage = payload.message;
      state.alertStatus = payload.status;
    },
    hideAlert: state => {
      // TODO: fix that
      state.isAlertHidden = true;
      state.alertMessage = '';
      state.alertStatus = 'unknown';
    },
  },
});

export const {
  actions: {privateShowAlert, hideAlert},
} = slice;

export const showAlert = payload => (dispatch, getState) => {
  const {isAlertHidden} = getState().alerts;
  if (!isAlertHidden) dispatch(hideAlert());
  return dispatch(privateShowAlert(payload));
};

export default slice.reducer;
