export const pendingReq = (state, {meta}) => {
  if (state.loading === 'idle') {
    state.loading = 'pending';
    state.currentRequestId = meta.requestId;
  }
};

export const finishReq = action => (state, {meta, payload}) => {
  if (
    state.loading === 'pending' &&
    state.currentRequestId === meta.requestId
  ) {
    state.loading = 'idle';
    state.currentRequestId = undefined;
    if (action && payload) action(state, payload);
  }
};

export const standardReq = (asyncThunk, action) => ({
  [asyncThunk.pending]: pendingReq,
  [asyncThunk.fulfilled]: finishReq(action),
  [asyncThunk.rejected]: finishReq(),
});
