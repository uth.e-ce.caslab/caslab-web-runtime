import {Dashboard, Projects, Group} from 'grommet-icons';

export default [
  {
    label: 'Dashboard',
    Icon: Dashboard,
    path: '/dashboard',
    adminOnly: false,
    userOnly: false,
  },
  {
    label: 'Projects',
    Icon: Projects,
    path: '/projects',
    adminOnly: false,
    userOnly: true,
  },
  {
    label: 'Users',
    Icon: Group,
    path: '/users',
    adminOnly: true,
    userOnly: false,
  },
];
