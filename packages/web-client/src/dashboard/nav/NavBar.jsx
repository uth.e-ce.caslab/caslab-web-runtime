import {Box, Text} from 'grommet';
import PropTypes from 'prop-types';
import React from 'react';
import {connect} from 'react-redux';
import {useHistory, useLocation} from 'react-router-dom';
import {menuActiveColor, menuColor} from '../style';
import routes from './routes';

const NavBar = ({
  rootPath,
  wrapperProps,
  elemProps,
  activeElemProps,
  iconStyle,
  isAdmin,
}) => {
  const {pathname} = useLocation();
  const {push} = useHistory();
  return (
    <Box background={menuColor} align="center" {...wrapperProps}>
      {routes.reduce((list, {label, Icon, path, adminOnly, userOnly}) => {
        if ((adminOnly && !isAdmin) || (userOnly && isAdmin)) return list;
        let finalActiveElemProps = {};
        let style = {outline: 'none'};
        if (pathname.match(`^${rootPath}${path}.*`)) {
          finalActiveElemProps = {
            background: menuActiveColor,
            elevation: 'medium',
            ...activeElemProps,
          };
          style.zIndex = 1;
        }
        list.push(
          <Box
            key={label}
            align="center"
            onClick={() => push(`${rootPath}${path}`)}
            focusIndicator={false}
            fill="horizontal"
            {...elemProps}
            {...finalActiveElemProps}
            style={style}
          >
            <Icon {...iconStyle} />
            <Text>{label}</Text>
          </Box>,
        );
        return list;
      }, [])}
    </Box>
  );
};
NavBar.propTypes = {
  rootPath: PropTypes.string.isRequired,
  wrapperProps: PropTypes.any.isRequired,
  elemProps: PropTypes.any.isRequired,
  activeElemProps: PropTypes.any.isRequired,
  iconStyle: PropTypes.any.isRequired,
  isAdmin: PropTypes.bool.isRequired,
};

NavBar.defaultProps = {
  wrapperProps: {},
  elemProps: {},
  activeElemProps: {},
  iconStyle: {},
};

const mapStateToProps = state => ({
  isAdmin: state.auth.isAdmin,
});

export default connect(mapStateToProps)(NavBar);
