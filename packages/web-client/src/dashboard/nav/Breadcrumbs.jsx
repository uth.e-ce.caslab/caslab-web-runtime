import {Box, Text} from 'grommet';
import PropTypes from 'prop-types';
import React from 'react';
import {FormNext} from 'grommet-icons';
import {Link} from 'react-router-dom';

function Breadcrumbs({steps}) {
  const currStep = steps[steps.length - 1];
  const prevSteps = steps.slice(0, steps.length - 1); // keep everything except current step
  const prevElements = prevSteps.reduce((list, step) => {
    const elems = [];
    if (step.Icon) elems.push(<step.Icon />);
    if (step.label) elems.push(<Text>{step.label}</Text>);
    list.push(<Link to={step.path}>{elems}</Link>);
    list.push(<FormNext />);
    return list;
  }, []);
  const currElement = <Text>{currStep.label}</Text>;
  return (
    <Box direction="row" pad="small" gap="xsmall">
      {prevElements}
      {currElement}
    </Box>
  );
}

Breadcrumbs.propTypes = {
  steps: PropTypes.array.isRequired,
};

export default Breadcrumbs;
