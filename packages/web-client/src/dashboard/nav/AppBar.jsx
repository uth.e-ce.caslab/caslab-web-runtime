import {Box} from 'grommet';
import React from 'react';
import {menuColor} from '../style';

const AppBar = props => (
  <Box
    direction="row"
    align="center"
    justify="between"
    background={menuColor}
    pad={{left: 'medium', right: 'small', vertical: 'small'}}
    elevation="medium"
    style={{zIndex: '2'}}
    {...props}
  />
);

export default AppBar;
