import {Box, Button, DataTable, Text, TextInput} from 'grommet';
import {
  Add,
  Checkbox,
  CheckboxSelected,
  Search,
  Send,
  Trash,
} from 'grommet-icons';
import PropTypes from 'prop-types';
import React, {useState} from 'react';
import {connect} from 'react-redux';
import {useHistory} from 'react-router-dom';
import ConfirmPopUp from '../../components/ConfirmPopUp';
import {unwrapResult} from '@reduxjs/toolkit';
import {showAlert as showAlertAct} from '../../store/state/alerts';
import {
  deleteUser as deleteUserAct,
  fetchUser as fetchUserAct,
} from '../../store/state/users';
import {TipContent} from '../../components';
import {api} from '../../utils';

function UsersList({users, usersPath, deleteUser, fetchUser, showAlert}) {
  const [filter, setFilter] = useState('');
  const finalUsers = users.filter(
    u => u.username.includes(filter) || u.email.includes(filter),
  );
  let history = useHistory();
  const [deleteDialogId, setDeleteDialogId] = useState('');
  const [verifyDialogId, setVerifyDialogId] = useState('');
  const [activateDialogData, setActiveDialogData] = useState(null);
  return (
    <Box flex fill gap="xsmall">
      <Box direction="row" gap="between">
        <TextInput
          plain
          icon={<Search />}
          placeholder="Search..."
          value={filter}
          onChange={e => setFilter(e.target.value)}
        />
        <Button
          icon={<Add />}
          focusIndicator
          hoverIndicator
          disabled
          onClick={() => history.push(`${usersPath}/new`)}
        />
      </Box>
      <Box background="light-1" elevation="small" flex fill pad="xsmall">
        <DataTable
          sortable={true}
          size="medium"
          columns={[
            {
              property: 'username',
              header: <Text>Username</Text>,
              primary: true,
              sortable: true,
              align: 'center',
              render: datum => (
                <Box>
                  <Text truncate={true}>{datum.username}</Text>
                </Box>
              ),
            },
            {
              property: 'email',
              header: <Text>Email</Text>,
              primary: true,
              sortable: true,
              align: 'center',
              render: ({email, emailVerified}) => (
                <Box direction="row">
                  <Text truncate={true}>{email}&nbsp;</Text>
                  {emailVerified ? <CheckboxSelected /> : <Checkbox />}
                </Box>
              ),
            },
            {
              property: 'active',
              header: <Text>Active</Text>,
              primary: true,
              sortable: true,
              align: 'center',
              render: ({id, active}) => (
                <Box>
                  <Button
                    icon={active ? <CheckboxSelected /> : <Checkbox />}
                    plain
                    focusIndicator={false}
                    onClick={e => {
                      e.stopPropagation(); // to disable row select
                      setActiveDialogData({id, active});
                    }}
                    tip={{
                      plain: true,
                      content: (
                        <TipContent
                          message={active ? 'Deactivate user' : 'Activate user'}
                        />
                      ),
                    }}
                  />
                </Box>
              ),
            },
            {
              property: 'id',
              header: 'Actions',
              sortable: false,
              align: 'center',
              render: datum => (
                <Box direction="row" justify="center" gap="xsmall">
                  <Button
                    icon={<Trash color="status-critical" />}
                    plain
                    focusIndicator={false}
                    onClick={e => {
                      e.stopPropagation(); // to disable row select
                      setDeleteDialogId(datum.id);
                    }}
                    tip={{
                      plain: true,
                      content: <TipContent message="Delete user" />,
                    }}
                  />
                  <Button
                    icon={<Send />}
                    plain
                    tip={{
                      plain: true,
                      content: (
                        <TipContent message="Request email verification" />
                      ),
                    }}
                    focusIndicator={false}
                    onClick={e => {
                      e.stopPropagation(); // to disable row select
                      setVerifyDialogId(datum.id);
                    }}
                  />
                </Box>
              ),
            },
          ]}
          data={finalUsers}
        />
        {!finalUsers.length && (
          <Text margin={{top: 'xsmall'}} textAlign="center">
            No matching users for this filter
          </Text>
        )}
        {deleteDialogId && (
          <ConfirmPopUp
            message="Are you sure you want to delete the user?"
            confirmMessage="Delete"
            confirmColor="status-critical"
            onClose={() => setDeleteDialogId('')}
            onConfirm={async () => {
              setDeleteDialogId('');
              const action = await deleteUser(deleteDialogId);
              try {
                unwrapResult(action);
                showAlert({
                  message: 'Deleted user successfully',
                  status: 'ok',
                });
              } catch (unused) {
                showAlert({
                  message: 'Delete user failed',
                  status: 'critical',
                });
              }
            }}
          />
        )}
        {verifyDialogId && (
          <ConfirmPopUp
            message="Request email verification?"
            confirmMessage="Send"
            confirmColor="status-ok"
            onClose={() => setVerifyDialogId('')}
            onConfirm={async () => {
              try {
                await api.post(`/users/${verifyDialogId}/verifyAgain`);
                await fetchUser(verifyDialogId);
                showAlert({
                  message: 'Requested email verification!',
                  status: 'ok',
                });
              } catch (unused) {
                showAlert({
                  message: 'Email verification request failed!',
                  status: 'critical',
                });
              }
              setVerifyDialogId('');
            }}
          />
        )}
        {activateDialogData && (
          <ConfirmPopUp
            message={`Are you sure you want to ${
              setActiveDialogData.active ? 'deactivate' : 'activate'
            } the user?`}
            confirmMessage="Continue"
            confirmColor="status-warning"
            onClose={() => setActiveDialogData(null)}
            onConfirm={async () => {
              const {id, active} = activateDialogData;
              try {
                await api.post(`/users/${id}/setActive`, {
                  active: !active,
                });
                await fetchUser(id);
                showAlert({
                  message: active ? 'Deactivated user!' : 'Activated user!',
                  status: 'ok',
                });
              } catch (unused) {
                showAlert({
                  message: active
                    ? 'User deactivation failed!'
                    : 'User activation failed!',
                  status: 'critical',
                });
              }
              setActiveDialogData(null);
            }}
          />
        )}
      </Box>
    </Box>
  );
}

UsersList.propTypes = {
  users: PropTypes.array.isRequired,
  usersPath: PropTypes.string.isRequired,
};

UsersList.mapActionsToProps = {
  deleteUser: deleteUserAct,
  fetchUser: fetchUserAct,
  showAlert: showAlertAct,
};

export default connect(null, UsersList.mapActionsToProps)(UsersList);
