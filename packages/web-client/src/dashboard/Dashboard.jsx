import {Box, Button, Heading} from 'grommet';
import {Logout, Notification} from 'grommet-icons';
import PropTypes from 'prop-types';
import React, {useState} from 'react';
import {connect} from 'react-redux';
import {Redirect, Route, Switch, useRouteMatch} from 'react-router-dom';
import {logoutAsync} from '../store/state/auth';
import {AppBar, BottomBar, SideBar} from './nav';
import {Notifications} from './notifications';
import {contentColor} from './style';
import {Users} from './users';
import {Projects} from './projects';
import DashboardIndex from './DashboardIndex';
import {useResponsive} from '../components';

function Dashboard({isAdmin, logoutAsync}) {
  const [showNotifications, setShowNotifications] = useState(false);
  const {isMobile} = useResponsive();
  const {path} = useRouteMatch();
  // this is done this way to avoid errors when dashboard's root path is '/'
  const rootPath = path === '/' ? '' : path;
  return (
    <Box fill>
      <AppBar>
        <Heading level="3" margin="none">
          CASlab
        </Heading>
        <Box direction="row" margin="none">
          <Button
            icon={<Notification />}
            onClick={() => setShowNotifications(!showNotifications)}
            disabled={true}
          />
          <Button icon={<Logout />} onClick={() => logoutAsync()} />
        </Box>
      </AppBar>
      <Box
        direction="row"
        flex
        overflow={{horizontal: 'hidden'}}
        onMouseUp={() => setShowNotifications(false)}
      >
        {!isMobile && <SideBar rootPath={rootPath} />}
        <Box
          flex
          align="start"
          justify="start"
          background={contentColor}
          pad="xsmall"
        >
          <Switch>
            <Route path={`${rootPath}/dashboard`} component={DashboardIndex} />
            {!isAdmin && (
              <Route path={`${rootPath}/projects`} component={Projects} />
            )}
            {isAdmin && <Route path={`${rootPath}/users`} component={Users} />}
            <Redirect to={`${rootPath}/dashboard`} />
          </Switch>
        </Box>
        <Notifications show={showNotifications} />
      </Box>
      {isMobile && <BottomBar rootPath={rootPath} />}
    </Box>
  );
}

Dashboard.propTypes = {
  logoutAsync: PropTypes.func.isRequired,
  isAdmin: PropTypes.bool.isRequired,
  username: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  isAdmin: state.auth.isAdmin,
  username: state.auth.username,
});

const mapActionsToProps = {
  logoutAsync,
};

export default connect(mapStateToProps, mapActionsToProps)(Dashboard);
