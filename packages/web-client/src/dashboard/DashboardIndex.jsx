import React, {useEffect, useState} from 'react';
import {Box, Heading, Card, CardBody, Grid, Text} from 'grommet';
import {PlayFill, PauseFill, Projects} from 'grommet-icons';
import api from '../utils/api';

const DashboardCard = ({title, value, color, background, Icon}) => (
  <Card background={background}>
    <CardBody pad="small">
      <Box pad="small" align="center" justify="center" fill>
        <Box direction="row" gap="medium" align="center">
          <Icon size="large" color={color} />
          <Heading size="medium" color={color}>
            {value}
          </Heading>
        </Box>
        <Text size="xlarge" weight="bold" color={color}>
          {title}
        </Text>
      </Box>
    </CardBody>
  </Card>
);

function DashboardIndex() {
  const [{all, idle, running}, setCounts] = useState({
    all: 0,
    idle: 0,
    running: 0,
  });
  useEffect(() => {
    api.get('/Projects/stats').then(({data}) => setCounts(data));
  });
  return (
    <Box fill>
      <Grid gap="small" rows="small" columns={{count: 'fit', size: 'small'}}>
        <DashboardCard
          title="All Projects"
          Icon={Projects}
          value={all}
          background="status-unknown"
          color="dark-1"
        />
        <DashboardCard
          title="Running Projects"
          Icon={PlayFill}
          value={running}
          background="status-ok"
          color="dark-1"
        />
        <DashboardCard
          title="Idle Projects"
          Icon={PauseFill}
          value={idle}
          background="status-critical"
          color="light-1"
        />
      </Grid>
    </Box>
  );
}

export default DashboardIndex;
