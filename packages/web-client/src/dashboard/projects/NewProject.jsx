import {unwrapResult} from '@reduxjs/toolkit';
import {Box, Button, Form, FormField, Heading, Layer, TextInput} from 'grommet';
import {Close, LinkPrevious} from 'grommet-icons';
import PropTypes from 'prop-types';
import React, {useState} from 'react';
import {connect} from 'react-redux';
import {createProject} from '../../store/state/projects';
import {showAlert} from '../../store/state/alerts';
import {useResponsive} from '../../components';

function NewProject({onExit, createProject, showAlert}) {
  const [errors, setErrors] = useState({});
  const {isMobile} = useResponsive();
  const titleEl = (
    <Heading level={2} margin="none" alignSelf="center">
      Add Project
    </Heading>
  );
  return (
    <Layer
      onEsc={onExit}
      onClickOutside={onExit}
      position="right"
      full="vertical"
    >
      <Form
        errors={errors}
        onSubmit={async ({value}) => {
          const action = await createProject(value);
          try {
            unwrapResult(action);
            showAlert({
              message: 'Created project successfully',
              status: 'ok',
            });
            onExit();
          } catch (unused) {
            const {details, message} = action.payload?.error || {};
            if (details) {
              const newErrors = details.reduce((o, {path, message}) => {
                const propName = path.replace('/', '');
                o[propName] = message;
                return o;
              }, {});
              setErrors(newErrors);
            } else if (message) {
              setErrors({});
              showAlert({message, status: 'critical'});
            }
          }
        }}
        style={{height: '100%'}}
      >
        <Box fill="vertical">
          {isMobile ? (
            <Box
              flex={false}
              direction="row"
              justify="between"
              elevation="small"
            >
              <Button icon={<LinkPrevious />} onClick={onExit} />
              <Box direction="row" fill="horizontal" justify="center">
                {titleEl}
              </Box>
              <Button
                icon={<LinkPrevious />}
                style={{visibility: 'hidden'}}
                disabled={true}
              />
            </Box>
          ) : (
            <Box
              flex={false}
              direction="row"
              justify="between"
              pad={{vertical: 'small', left: 'small', right: 'xsmall'}}
            >
              {titleEl}
              <Button icon={<Close />} onClick={onExit} />
            </Box>
          )}
          <Box
            pad="medium"
            justify={isMobile ? 'between' : 'start'}
            fill={isMobile ? 'vertical' : false}
          >
            <Box flex="grow">
              <FormField name="title" label="Title">
                <TextInput name="title" />
              </FormField>
              <FormField name="description" label="Description">
                <TextInput name="description" />
              </FormField>
            </Box>
            <Box flex={false} align="start" margin={{top: 'small'}}>
              <Button type="submit" label="Submit" primary />
            </Box>
          </Box>
        </Box>
      </Form>
    </Layer>
  );
}

NewProject.propTypes = {
  onExit: PropTypes.func.isRequired,
  createProject: PropTypes.func.isRequired,
  showAlert: PropTypes.func.isRequired,
};

const mapActionsToProps = {createProject, showAlert};

export default connect(null, mapActionsToProps)(NewProject);
