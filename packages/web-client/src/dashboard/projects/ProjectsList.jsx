import {unwrapResult} from '@reduxjs/toolkit';
import {Box, Button, DataTable, Text, TextInput} from 'grommet';
import {Add, Search, Trash} from 'grommet-icons';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {useHistory} from 'react-router-dom';
import {deleteProject as deleteUserAct} from '../../store/state/projects';
import {showAlert as showAlertAct} from '../../store/state/alerts';
import ConfirmPopUp from '../../components/ConfirmPopUp';
import NewProject from './NewProject';
import {strOrEmpty} from '../../utils';
import {TipContent} from '../../components';

const searchFields = ['id', 'name', 'type', 'user.username'];
const allColumns = [
  {
    property: 'title',
    header: <Text>Title</Text>,
    primary: true,
    sortable: true,
    align: 'center',
    render: datum => (
      <Box width={{max: 'medium'}}>
        <Text truncate>{strOrEmpty(datum.title)}</Text>
      </Box>
    ),
  },
  {
    property: 'state',
    header: <Text>State</Text>,
    primary: true,
    sortable: true,
    align: 'center',
    render: datum => (
      <Box>
        <Text truncate>{datum.state}</Text>
      </Box>
    ),
  },
  {
    property: 'userId',
    header: <Text>User</Text>,
    primary: true,
    sortable: true,
    align: 'center',
    render: datum => (
      <Box>
        <Text truncate={true}>{strOrEmpty(datum.user?.username)}</Text>
      </Box>
    ),
  },
];

function ProjectsList({
  projects,
  projectsPath,
  isAdmin,
  deleteProject,
  showAlert,
}) {
  const history = useHistory();
  const [filters, setFilters] = useState({
    search: '',
  });
  const [filteredProjects, setFilteredProjects] = useState(projects);
  useEffect(() => {
    setFilteredProjects(
      projects.filter(dev =>
        searchFields.some(
          f =>
            _.has(dev, f) &&
            _.get(dev, f).toLowerCase().includes(filters.search.toLowerCase()),
        ),
      ),
    );
  }, [projects, filters]);
  const [showNew, setShowNew] = useState(false);
  const [deleteDialogId, setDeleteDialogId] = useState('');
  const [columns, setColumns] = useState(allColumns);
  useEffect(() => {
    if (!isAdmin) {
      setColumns(allColumns.filter(({property}) => property !== 'userId'));
    }
  }, [isAdmin]);
  return (
    <Box flex fill gap="xsmall">
      <Box direction="row" gap="between">
        <Box direction="row" fill="horizontal">
          <TextInput
            plain
            icon={<Search />}
            placeholder="Search..."
            value={filters.search}
            onChange={({target: {value}}) =>
              setFilters({
                ...filters,
                search: value,
              })
            }
            style={{height: '100%'}}
          />
        </Box>
        <Button
          icon={<Add />}
          focusIndicator
          hoverIndicator
          onClick={() => setShowNew(true)}
        />
      </Box>
      <Box
        background="light-1"
        elevation="small"
        flex
        fill
        pad="xsmall"
        overflow="scroll"
      >
        <DataTable
          sortable={true}
          columns={[
            ...columns,
            {
              property: 'actions',
              header: 'Actions',
              sortable: false,
              align: 'center',
              render: datum => (
                <Box direction="row" justify="center">
                  <Button
                    icon={<Trash color="status-critical" />}
                    plain={true}
                    focusIndicator={false}
                    onClick={e => {
                      e.stopPropagation(); // to disable row select
                      setDeleteDialogId(datum.id);
                    }}
                    tip={{
                      plain: true,
                      content: <TipContent message={'Delete project'} />,
                    }}
                  />
                </Box>
              ),
            },
          ]}
          onClickRow={({datum}) => history.push(`${projectsPath}/${datum.id}`)}
          data={filteredProjects}
        />
        {(!projects.length && (
          <Text margin={{top: 'xsmall'}} textAlign="center">
            No projects
          </Text>
        )) ||
          (!filteredProjects.length && (
            <Text margin={{top: 'xsmall'}} textAlign="center">
              No matching projects for this filter
            </Text>
          ))}
        {deleteDialogId && (
          <ConfirmPopUp
            message="Are you sure you want to delete?"
            confirmMessage="Delete"
            confirmColor="status-critical"
            onClose={() => setDeleteDialogId('')}
            onConfirm={async () => {
              setDeleteDialogId('');
              const action = await deleteProject(deleteDialogId);
              try {
                unwrapResult(action);
                showAlert({
                  message: 'Deleted project successfully',
                  status: 'ok',
                });
              } catch (unused) {
                showAlert({
                  message: 'Delete project failed',
                  status: 'critical',
                });
              }
            }}
          />
        )}
        {showNew && <NewProject onExit={() => setShowNew(false)} />}
      </Box>
    </Box>
  );
}

ProjectsList.propTypes = {
  projects: PropTypes.array.isRequired,
  projectsPath: PropTypes.string.isRequired,
};

ProjectsList.mapActionsToProps = {
  deleteProject: deleteUserAct,
  showAlert: showAlertAct,
};

ProjectsList.mapStateToProps = state => ({
  projects: Object.values(state.projects.entities),
});

export default connect(
  ProjectsList.mapStateToProps,
  ProjectsList.mapActionsToProps,
)(ProjectsList);
