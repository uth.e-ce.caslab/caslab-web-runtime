import {Box, Button, Heading, Text, TextInput} from 'grommet';
import PropTypes from 'prop-types';
import React, {useState} from 'react';
import {Document, DocumentDownload, DocumentUpload} from 'grommet-icons';
import {connect} from 'react-redux';
import {showAlert as showAlertAct} from '../../../store/state/alerts';
import {api} from '../../../utils';

function ProjectFiles({project, showAlert, ...extra}) {
  const [file, setFile] = useState(null);
  const [path, setPath] = useState('');
  let tileStyle;
  switch (project.state) {
    case 'RUNNING':
      // nothing
      break;
    default:
      tileStyle = {
        pointerEvents: 'none', // makes tile elements un-clickable
        opacity: 0.5, // makes tile look like it is disabled
      };
  }
  return (
    <Box
      elevation="small"
      pad="small"
      width={{min: 'medium'}}
      style={tileStyle}
      {...extra}
    >
      <Box direction="row" align="center" justify="center" gap="xsmall">
        <Document size="medium" />
        <Heading
          level="3"
          size="medium"
          textAlign="center"
          style={{margin: '4px'}}
        >
          File Operations
        </Heading>
      </Box>
      <Box direction="row" justify="between" align="center" gap="small">
        <Text weight="bold">Path</Text>
        <TextInput
          id="file-path"
          onChange={event => setPath(event.target.value)}
        />
        <Button
          icon={<DocumentDownload />}
          size="small"
          style={{padding: '4px', borderRadius: 0}}
          onClick={() =>
            api
              .getFile(`/Projects/${project.id}/download`, {path})
              .then(() =>
                showAlert({
                  message: `Downloaded ${path}!`,
                  status: 'ok',
                }),
              )
              .catch(() =>
                showAlert({
                  message: `Download ${path} failed!`,
                  status: 'critical',
                }),
              )
          }
          disabled={!path}
        />
      </Box>
      <Box direction="row" justify="between" align="center">
        <input
          type="file"
          name="file"
          onChange={e => setFile(e.target.files[0])}
        />
        <Button
          icon={<DocumentUpload />}
          size="small"
          style={{padding: '4px', borderRadius: 0}}
          onClick={() =>
            api
              .postFile(`/Projects/${project.id}/upload`, {path, file})
              .then(() =>
                showAlert({
                  message: `Uploaded ${file.name} to ${path}!`,
                  status: 'ok',
                }),
              )
              .catch(() =>
                showAlert({
                  message: `Upload ${file.name} failed!`,
                  status: 'critical',
                }),
              )
          }
          disabled={!path || !file}
        />
      </Box>
    </Box>
  );
}

ProjectFiles.propTypes = {
  project: PropTypes.object.isRequired,
  showAlert: PropTypes.func.isRequired,
};

ProjectFiles.mapActionsToProps = {
  showAlert: showAlertAct,
};

export default connect(null, ProjectFiles.mapActionsToProps)(ProjectFiles);
