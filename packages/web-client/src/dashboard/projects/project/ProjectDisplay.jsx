import {Box, Text} from 'grommet';
import PropTypes from 'prop-types';
import React from 'react';
import {Heading} from 'grommet';
import {Monitor} from 'grommet-icons';
import Loading from '../../../components/Loading';

function ProjectDisplay({project}) {
  if (true) {
    return null;
  }
  // TODO: implement this
  return (
    <Box elevation="small" pad="small" flex="grow">
      <Box direction="row" align="center" justify="center" gap="xsmall">
        <Monitor size="medium" />
        <Heading
          level="3"
          size="medium"
          textAlign="center"
          style={{margin: '4px'}}
        >
          Display
        </Heading>
      </Box>
      <Box
        height={{min: 'small'}}
        justify="center"
        border
        background="light-5"
        fill
      >
        <Box align="center">
          {project.state === 'RUNNING' ? (
            <Loading />
          ) : (
            <Box animation={{type: 'pulse', duration: 500}}>
              <Text textAlign="center">Waiting for project to start!</Text>
            </Box>
          )}
        </Box>
      </Box>
    </Box>
  );
}

ProjectDisplay.propTypes = {
  project: PropTypes.object.isRequired,
};

export default ProjectDisplay;
