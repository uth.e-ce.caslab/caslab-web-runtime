import {Box, Text} from 'grommet';
import PropTypes from 'prop-types';
import React, {useContext, useEffect, useState} from 'react';
import {Heading} from 'grommet';
import {Cli} from 'grommet-icons';
import {Terminal} from 'xterm';
import {FitAddon} from 'xterm-addon-fit';
import {WebLinksAddon} from 'xterm-addon-web-links';
import {SearchAddon} from 'xterm-addon-search';
import 'xterm/css/xterm.css';
import {connect} from 'react-redux';
import {localSetProjectState as localSetProjectStateAct} from '../../../store/state/projects';
import {WebSocketContext} from '../../../socket/WebSocket';

function createTerminal() {
  const term = new Terminal({
    theme: {background: '#333333'}, // dark-1=#333333
  });
  term.addons = {
    fit: new FitAddon(),
    webLinks: new WebLinksAddon(),
    search: new SearchAddon(),
  };
  term.loadAddon(term.addons.fit);
  term.loadAddon(term.addons.webLinks);
  term.loadAddon(term.addons.search);
  const element = document.getElementById('terminal');
  term.open(element);
  term.addons.fit.fit();
  return term;
}

function ProjectShell({project, wrapperProps, localSetProjectState}) {
  const feeds = useContext(WebSocketContext);
  const [term, setTerminal] = useState(null);
  useEffect(() => {
    if (project.state === 'RUNNING') {
      setTerminal(createTerminal());
    } else {
      setTerminal(null);
    }
  }, [project.state]);
  useEffect(() => {
    if (!feeds || !term) return; // do nothing if no feeds or no term
    const {projects} = feeds;
    const handleStateChange = state =>
      localSetProjectState({projectId: project.id, state});
    const handleShellOutput = data => term.write(data);
    projects.on('state', handleStateChange);
    projects.on('shellOutput', handleShellOutput);
    term.onData(data => projects.emit('shellInput', project.id, data));
    return () => {
      projects.removeListener('state', handleStateChange);
      projects.removeListener('shellOutput', handleShellOutput);
    };
  }, [feeds, localSetProjectState, project.id, term]);
  return (
    <Box
      elevation="small"
      pad="small"
      fill="horizontal"
      background="dark-1"
      {...wrapperProps}
    >
      <Box direction="row" align="center" justify="center" gap="xsmall">
        <Cli size="medium" />
        <Heading
          level="3"
          size="medium"
          textAlign="center"
          style={{margin: '4px'}}
        >
          Shell
        </Heading>
      </Box>
      <Box
        fill="vertical"
        width={{min: 'medium'}}
        height={{min: 'small'}}
        overflow="scroll"
        className="show-scrollbar"
      >
        {project.state === 'RUNNING' && <Box fill id="terminal" />}
        {project.state !== 'RUNNING' && (
          <Box
            align="center"
            fill
            justify="center"
            animation={{type: 'pulse', duration: 500}}
          >
            <Text textAlign="center">Waiting for project to start!</Text>
          </Box>
        )}
      </Box>
    </Box>
  );
}

ProjectShell.propTypes = {
  project: PropTypes.object.isRequired,
  wrapperProps: PropTypes.object,
  localSetProjectState: PropTypes.func.isRequired,
};

ProjectShell.mapActionsToProps = {
  localSetProjectState: localSetProjectStateAct,
};

export default connect(null, ProjectShell.mapActionsToProps)(ProjectShell);
