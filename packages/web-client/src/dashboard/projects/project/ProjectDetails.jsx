import {Box, Heading} from 'grommet';
import PropTypes from 'prop-types';
import React from 'react';
import {CircleInformation} from 'grommet-icons';
import {connect} from 'react-redux';
import {HorizontalLine, TextFieldElement} from '../../../components';
import ProjectMeta from './ProjectMeta';
import StateButton from './StateButton';
import EditableTextField from '../../../components/EditableTextField';
import {updateProject as updateProjectAct} from '../../../store/state/projects';
import {showAlert as showAlertAct} from '../../../store/state/alerts';

function ProjectDetails({project, showAlert, updateProject, ...extra}) {
  return (
    <Box elevation="small" pad="small" width={{min: 'medium'}} {...extra}>
      <Box direction="row" align="center" justify="center" gap="xsmall">
        <CircleInformation size="medium" />
        <Heading
          level="3"
          size="medium"
          textAlign="center"
          style={{margin: '4px'}}
        >
          Information
        </Heading>
      </Box>
      <EditableTextField
        initialValue={project.description}
        save={newValue =>
          updateProject({
            id: project.id,
            data: {description: newValue},
            reload: false,
          }).then(() =>
            showAlert({
              message: 'Updated description!',
              status: 'ok',
            }),
          )
        }
        textProps={{textAlign: 'start', placeholder: '<Empty Description>'}}
        multiline
      />
      <HorizontalLine />
      <Box direction="row" gap="small" justify="between" align="center">
        <TextFieldElement title="State" value={project.state} />
        <Box direction="row">
          <StateButton project={project} />
        </Box>
      </Box>
      <HorizontalLine />
      <ProjectMeta project={project} />
    </Box>
  );
}

ProjectDetails.propTypes = {
  project: PropTypes.object.isRequired,
};

ProjectDetails.mapActionsToProps = {
  updateProject: updateProjectAct,
  showAlert: showAlertAct,
};

export default connect(
  ProjectDetails.mapStateToProps,
  ProjectDetails.mapActionsToProps,
)(ProjectDetails);
