import {Box, Layer, Button, Heading} from 'grommet';
import {LinkPrevious} from 'grommet-icons';
import PropTypes from 'prop-types';
import React from 'react';
import {connect} from 'react-redux';
import {Redirect, useHistory} from 'react-router-dom';

import ProjectDetails from './ProjectDetails';
import ProjectShell from './ProjectShell';
import ProjectDisplay from './ProjectDisplay';
import ProjectFiles from './ProjectFiles';
import EditableTextField from '../../../components/EditableTextField';
import {updateProject as updateProjectAct} from '../../../store/state/projects';
import {showAlert as showAlertAct} from '../../../store/state/alerts';
import {useProjectFeed} from '../../../socket';
import {useResponsive} from '../../../components';

function Project({projectId, project, projectsPath, updateProject, showAlert}) {
  const history = useHistory();
  const {size, isMobile} = useResponsive();
  useProjectFeed(projectId);
  if (!project) return <Redirect to={projectsPath} />;
  const titleEl = (
    <Box
      direction="row"
      fill="horizontal"
      justify="center"
      height={{min: '24px'}}
    >
      <Heading
        level={2}
        margin="xxsmall"
        alignSelf="center"
        textAlign="center"
        style={{maxWidth: 'none'}}
      >
        <EditableTextField
          initialValue={project.title}
          textProps={{placeholder: '<Empty Title>'}}
          save={newValue =>
            updateProject({
              id: project.id,
              data: {title: newValue},
              reload: false,
            }).then(() =>
              showAlert({
                message: 'Updated title!',
                status: 'ok',
              }),
            )
          }
        />
      </Heading>
    </Box>
  );
  return isMobile ? (
    <Layer>
      <Box fill="vertical">
        <Box flex={false} direction="row" justify="between" elevation="small">
          <Button
            icon={<LinkPrevious />}
            onClick={() => history.push(projectsPath)}
          />
          {titleEl}
          <Button
            icon={<LinkPrevious />}
            style={{visibility: 'hidden'}}
            disabled={true}
          />
        </Box>
        <Box
          fill="horizontal"
          overflow="scroll"
          direction="column"
          gap="xsmall"
          flex="grow"
        >
          <ProjectDetails project={project} fill="horizontal" />
          <ProjectDisplay project={project} />
          <ProjectShell project={project} />
        </Box>
      </Box>
    </Layer>
  ) : (
    <Box fill gap="xsmall" overflow="scroll">
      {titleEl}
      <Box
        direction={size === 'medium' ? 'column' : 'row'}
        gap="xsmall"
        flex="grow"
      >
        <Box direction="column" gap="xsmall">
          <ProjectDetails project={project} />
          <ProjectFiles project={project} />
        </Box>
        <Box flex="grow" gap="xsmall">
          <ProjectDisplay project={project} />
          <ProjectShell project={project} wrapperProps={{fill: true}} />
        </Box>
      </Box>
    </Box>
  );
}

Project.propTypes = {
  project: PropTypes.object.isRequired,
  projectsPath: PropTypes.string.isRequired,
};

Project.mapStateToProps = (state, ownProps) => ({
  project: state.projects.entities[ownProps.projectId],
});

Project.mapActionsToProps = {
  updateProject: updateProjectAct,
  showAlert: showAlertAct,
};

export default connect(
  Project.mapStateToProps,
  Project.mapActionsToProps,
)(Project);
