import {Box, Button} from 'grommet';
import PropTypes from 'prop-types';
import React from 'react';
import {PauseFill, PlayFill} from 'grommet-icons';
import {connect} from 'react-redux';
import {setProjectState as setProjectStateAct} from '../../../store/state/projects';
import {showAlert as showAlertAct} from '../../../store/state/alerts';
import {asyncActionGenerator} from '../../../utils';
import {TipContent} from '../../../components';

function StateButton({project, setProjectState, showAlert}) {
  const {state} = project;
  let Icon;
  let color;
  let action;
  let animation = undefined;
  let tip;
  switch (state) {
    case 'IDLE':
      Icon = PlayFill;
      color = 'status-ok';
      tip = 'Start project';
      action = asyncActionGenerator(
        setProjectState,
        {id: project.id, state: 'RUNNING'},
        showAlert,
        tip,
      );
      animation = {type: 'pulse', duration: 500};
      break;
    case 'RUNNING':
      Icon = PauseFill;
      color = 'status-critical';
      tip = 'Stop project';
      action = asyncActionGenerator(
        setProjectState,
        {id: project.id, state: 'IDLE'}, // TODO: add prompt before action
        showAlert,
        tip,
      );
      break;
    default:
      return null;
  }
  return (
    <Box animation={animation}>
      <Button
        icon={<Icon color={color} />}
        size="small"
        plain={false}
        style={{padding: '4px', borderRadius: 0}}
        onClick={action}
        color={color}
        tip={{
          plain: true,
          content: <TipContent message={tip} />,
        }}
      />
    </Box>
  );
}

StateButton.propTypes = {
  project: PropTypes.object.isRequired,
  setProjectState: PropTypes.func.isRequired,
  showAlert: PropTypes.func.isRequired,
};

StateButton.mapActionsToProps = {
  setProjectState: setProjectStateAct,
  showAlert: showAlertAct,
};

export default connect(null, StateButton.mapActionsToProps)(StateButton);
