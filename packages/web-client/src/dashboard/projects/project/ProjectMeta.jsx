import {Box} from 'grommet';
import PropTypes from 'prop-types';
import React from 'react';
import moment from 'moment';
import TextFieldElement from '../../../components/TextFieldElement';

function ProjectMeta({project, ...extra}) {
  return (
    <Box direction="column" gap="small" justify="between" {...extra}>
      {project.createdAt && (
        <TextFieldElement
          title="Created At"
          value={moment(project.createdAt).calendar()}
        />
      )}
      {project.updatedAt && (
        <TextFieldElement
          title="Updated At"
          value={moment(project.updatedAt).calendar()}
        />
      )}
      {project.startedAt && (
        <TextFieldElement
          title="Started At"
          value={moment(project.startedAt).calendar()}
        />
      )}
      {project.stoppedAt && (
        <TextFieldElement
          title="Stopped At"
          value={moment(project.stoppedAt).calendar()}
        />
      )}
    </Box>
  );
}

ProjectMeta.propTypes = {
  project: PropTypes.object.isRequired,
};

export default ProjectMeta;
