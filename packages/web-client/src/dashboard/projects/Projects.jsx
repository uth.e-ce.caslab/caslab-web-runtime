import PropTypes from 'prop-types';
import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Route, Switch, useRouteMatch} from 'react-router-dom';
import {fetchProjects} from '../../store/state/projects';
import Project from './project/Project';
import ProjectsList from './ProjectsList';
import Loading from '../../components/Loading';

function Projects(props) {
  const {fetchProjects} = props;
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    fetchProjects().then(() => setLoading(false));
  }, [fetchProjects]);
  const {path} = useRouteMatch();
  return loading ? (
    <Loading size="xlarge" />
  ) : (
    <Switch>
      <Route exact path={path}>
        <ProjectsList projectsPath={path} />
      </Route>
      <Route
        path={`${path}/:projectId`}
        component={({match}) => (
          <Project projectId={match.params.projectId} projectsPath={path} />
        )}
      />
    </Switch>
  );
}

Projects.propTypes = {
  fetchProjects: PropTypes.func.isRequired,
};

export default connect(null, {fetchProjects})(Projects);
