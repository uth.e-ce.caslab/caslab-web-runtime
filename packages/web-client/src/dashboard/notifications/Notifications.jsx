import {Box, Collapsible} from 'grommet';
import PropTypes from 'prop-types';
import React from 'react';

const Notifications = ({show}) => (
  <Collapsible direction="horizontal" open={show}>
    <Box
      background="light-2"
      elevation="small"
      align="center"
      justify="start"
      flex
    >
      {['notification 1', 'notification 2'].map((t, i) => (
        <Box
          key={i}
          elevation="small"
          pad={{horizontal: 'large', vertical: 'small'}}
          align="center"
        >
          {t}
        </Box>
      ))}
    </Box>
  </Collapsible>
);

Notification.propTypes = {
  show: PropTypes.bool.isRequired,
};

export default Notifications;
