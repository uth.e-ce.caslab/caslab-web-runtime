import {Box, Text} from 'grommet';
import PropTypes from 'prop-types';
import React from 'react';

const TipContent = ({message}) => (
  <Box direction="column" align="center">
    <svg width="20px" height="12px">
      <polygon fill="grey" points="0,12 10,0 20,12" />
    </svg>
    <Box background={{color: 'grey'}} pad="small" round="xsmall">
      <Text color="white">{message}</Text>
    </Box>
  </Box>
);

TipContent.propTypes = {
  message: PropTypes.string.isRequired,
};

export default TipContent;
