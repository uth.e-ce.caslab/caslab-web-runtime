import React from 'react';
import {Box} from 'grommet/index';
import {InProgress} from 'grommet-icons/index';

const Loading = ({...extra}) => (
  <Box fill align="center" justify="center">
    <Box animation={{type: 'rotateRight', duration: 2000}}>
      <InProgress size="large" {...extra} />
    </Box>
  </Box>
);
export default Loading;
