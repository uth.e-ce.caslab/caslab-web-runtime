import {Box, Text} from 'grommet';
import PropTypes from 'prop-types';
import React from 'react';

const TextFieldElement = ({title, value}) => (
  <Box direction="row">
    <Text weight="bold">{title}:&nbsp;</Text>
    <Text>{value}</Text>
  </Box>
);
TextFieldElement.propTypes = {
  title: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};
export default TextFieldElement;
