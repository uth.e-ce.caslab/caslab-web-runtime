import React from 'react';
import {ResponsiveContext} from 'grommet';

const useResponsive = () => {
  const size = React.useContext(ResponsiveContext);
  let isMobile = false;
  if (size === 'small') {
    isMobile = true;
  }
  return {size, isMobile};
};

export default useResponsive;
