import {Box, TextArea, TextInput} from 'grommet';
import PropTypes from 'prop-types';
import React, {useCallback, useState} from 'react';

const EditableTextField = ({initialValue, save, textProps, multiline}) => {
  const [value, setValue] = useState(initialValue);
  const [focused, setFocused] = useState(false);
  const onBlur = useCallback(() => {
    setFocused(false);
    if (initialValue !== value) {
      // value change, so need to be saved
      save(value);
    }
  }, [initialValue, save, value]);
  let TextEl;
  let extraProps = {};
  if (multiline) {
    TextEl = TextArea;
    extraProps.resize = false;
  } else {
    TextEl = TextInput;
  }
  return (
    <Box direction="row" align="center" justify="center">
      <TextEl
        textAlign="center"
        plain={!focused}
        value={value}
        onChange={e => setValue(e.target.value)}
        onFocus={() => setFocused(true)}
        onBlur={onBlur}
        {...extraProps}
        {...textProps}
      />
    </Box>
  );
};
EditableTextField.propTypes = {
  initialValue: PropTypes.string.isRequired,
  save: PropTypes.func.isRequired,
  textProps: PropTypes.object,
  multiline: PropTypes.bool.isRequired,
};
EditableTextField.defaultProps = {
  multiline: false,
};
export default EditableTextField;
