import React from 'react';
import {Box} from 'grommet';
import PropTypes from 'prop-types';
import Loading from './Loading';
import {Button} from 'grommet';

const LoadingButton = ({loading, ...buttonProps}) => {
  let loadingProps = {};
  if (loading) {
    loadingProps.disabled = true;
    loadingProps.label = '';
    loadingProps.icon = (
      <Box>
        <Loading size="medium" color="white" />
      </Box>
    );
  }
  return <Button {...buttonProps} {...loadingProps} />;
};
LoadingButton.propTypes = {
  loading: PropTypes.bool.isRequired,
};
export default LoadingButton;
