import {Box, Button, Heading, Layer, Text} from 'grommet';
import PropTypes from 'prop-types';
import React, {useState} from 'react';

const ConfirmPopUp = ({
  title,
  message,
  confirmColor,
  confirmMessage,
  closeMessage,
  onClose,
  onConfirm,
}) => {
  const [pending, setPending] = useState(false);
  return (
    <Layer
      position="center"
      modal={true}
      responsive={false}
      onEsc={onClose}
      onClickOutside={onClose}
    >
      <Box pad="medium" gap="small" width={{min: 'medium'}}>
        <Heading level={3} margin="none">
          {title}
        </Heading>
        <Text>{message}</Text>
        <Box
          as="footer"
          gap="small"
          direction="row"
          align="center"
          justify="end"
          pad={{top: 'medium'}}
        >
          <Button
            label={<strong>{closeMessage}</strong>}
            onClick={onClose}
            color="dark-3"
          />
          <Button
            label={<strong>{confirmMessage}</strong>}
            onClick={() => {
              setPending(true);
              return onConfirm();
            }}
            primary
            color={confirmColor}
            disabled={pending}
          />
        </Box>
      </Box>
    </Layer>
  );
};

ConfirmPopUp.propTypes = {
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  confirmColor: PropTypes.string.isRequired,
  confirmMessage: PropTypes.string.isRequired,
  closeMessage: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
};
ConfirmPopUp.defaultProps = {
  title: 'Confirm',
  message: '',
  confirmColor: '',
  confirmMessage: 'Confirm',
  closeMessage: 'Cancel',
};
export default ConfirmPopUp;
