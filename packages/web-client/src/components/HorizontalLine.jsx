import PropTypes from 'prop-types';
import React from 'react';

const HorizontalLine = ({color}) => (
  <hr
    style={{
      borderColor: color,
      width: '100%',
      background: color,
    }}
  />
);
HorizontalLine.propTypes = {
  color: PropTypes.string.isRequired,
};
HorizontalLine.defaultProps = {
  color: 'black',
};
export default HorizontalLine;
