import config from './config';
import Auth from './Auth';
import Login from './Login';
import Register from './Register';
import Verified from './Verified';
import PrivateRoute from './PrivateRoute';
import PublicOnlyRoute from './PublicOnlyRoute';
export * from './config';
export {PrivateRoute, PublicOnlyRoute, Auth, Login, Register, Verified};

export default {
  ...config,
  PrivateRoute,
  PublicOnlyRoute,
  Login,
  Register,
  Verified,
};
