// contains the auth configuration options

export const authRoute = '/auth',
  loginRoute = `${authRoute}/login`,
  registerRoute = `${authRoute}/register`,
  routeAfterAuth = '/dashboard',
  routeAfterUnauth = loginRoute;

export default {
  routeAfterAuth,
  routeAfterUnauth,
  loginRoute,
  registerRoute,
};
