import {Anchor, Box, Text} from 'grommet';
import React from 'react';
import {useHistory} from 'react-router-dom';
import {loginRoute} from './config';

const Verified = () => {
  const history = useHistory();
  return (
    <Box gap="small">
      <Text color="brand" weight="bold" size="large">
        Thank you for verifying your email address!
      </Text>
      <Text>
        Your new account has been activated and you can now&nbsp;
        <Anchor label="sign in" onClick={() => history.push(loginRoute)} />
        &nbsp;with your username and password.
      </Text>
    </Box>
  );
};
export default Verified;
