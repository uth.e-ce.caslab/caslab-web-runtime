import {Anchor, Box, Button, Form, FormField, Text, TextInput} from 'grommet';
import {Hide, Login as LoginIcon, View} from 'grommet-icons';
import PropTypes from 'prop-types';
import React, {useState} from 'react';
import {connect} from 'react-redux';
import {useHistory} from 'react-router-dom';
import {loginAsync as loginAsyncAct} from '../store/state/auth';
import {registerRoute} from './config';
import {
  showAlert as showAlertAct,
  hideAlert as hideAlertAct,
} from '../store/state/alerts';

function decodeError(err) {
  switch (err.code) {
    case 'LOGIN_FAILED_EMAIL_NOT_VERIFIED':
      return 'You need to confirm your email!';
    case 'DEACTIVATED_USER':
      return 'Your account is deactivated. Please contact the administration.';
    default:
      return 'Sign in failed!';
  }
}

const Login = ({showAlert, hideAlert, loginAsync}) => {
  const [showPass, setShowPass] = useState(false);
  const history = useHistory();
  let passType;
  let ShowPassIcon;
  if (showPass) {
    passType = 'text';
    ShowPassIcon = Hide;
  } else {
    passType = 'password';
    ShowPassIcon = View;
  }
  return (
    <Box>
      <Box>
        <Form
          onSubmit={({value}) => {
            hideAlert();
            loginAsync({...value}).catch(err =>
              showAlert({message: decodeError(err), status: 'critical'}),
            );
          }}
        >
          <FormField name="username" label="Username" required>
            <TextInput name="username" />
          </FormField>
          <FormField name="password" label="Password" required>
            <Box direction="row" align="center">
              <TextInput name="password" type={passType} plain />
              <Button
                icon={<ShowPassIcon size="medium" />}
                onClick={() => setShowPass(!showPass)}
                focusIndicator={false}
              />
            </Box>
          </FormField>
          <Button
            margin={{top: 'small'}}
            type="submit"
            primary
            label="Sign in"
            fill="horizontal"
            icon={<LoginIcon />}
          />
        </Form>
      </Box>
      <Box margin={{top: 'small'}} align="center">
        <Text>
          Don't have an account yet?&nbsp;
          <Anchor
            label="Register now"
            onClick={() => history.push(registerRoute)}
          />
        </Text>
      </Box>
    </Box>
  );
};
Login.propTypes = {
  loginAsync: PropTypes.func.isRequired,
  showAlert: PropTypes.func.isRequired,
  hideAlert: PropTypes.func.isRequired,
};
Login.mapActionsToProps = {
  loginAsync: loginAsyncAct,
  showAlert: showAlertAct,
  hideAlert: hideAlertAct,
};
export default connect(null, Login.mapActionsToProps)(Login);
