import {Box, grommet, Heading, ThemeContext} from 'grommet';
import {deepMerge} from 'grommet/utils';
import React from 'react';
import {
  authRoute,
  Login,
  Register,
  Verified,
  loginRoute,
  registerRoute,
} from './';
import {Redirect, Route, Switch} from 'react-router-dom';

const formTheme = deepMerge(grommet, {
  formField: {
    border: {position: 'inner', side: 'all'},
  },
});

const Auth = () => {
  return (
    <Box direction="row" justify="center" background="light-2" fill>
      <Box pad="medium">
        <ThemeContext.Extend value={formTheme}>
          <Box direction="row" justify="center">
            <Heading color="brand">CAS</Heading>
            <Heading color="dark-4">lab</Heading>
          </Box>
          <Switch>
            <Route path={loginRoute}>
              <Login />
            </Route>
            <Route path={registerRoute}>
              <Register />
            </Route>
            <Route path={`${authRoute}/verified`}>
              <Verified />
            </Route>
            <Redirect to="/" />
          </Switch>
        </ThemeContext.Extend>
      </Box>
    </Box>
  );
};
export default Auth;
