import {Anchor, Box, Button, Form, FormField, Text, TextInput} from 'grommet';
import {Hide, Sign as RegisterIcon, View} from 'grommet-icons';
import React, {useState} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router-dom';
import {api} from '../utils';
import {loginRoute} from './config';
import {
  hideAlert as hideAlertAct,
  showAlert as showAlertAct,
} from '../store/state/alerts';
import {LoadingButton} from '../components';

function decodeError(err) {
  const [firstMessage] = Object.values(err?.details?.messages || {}) || [];
  if (firstMessage) return firstMessage;
  return 'Sign up failed!';
}

const Register = ({showAlert, hideAlert}) => {
  const [showPass, setShowPass] = useState(false);
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  let passType;
  let ShowPassIcon;
  if (showPass) {
    passType = 'text';
    ShowPassIcon = Hide;
  } else {
    passType = 'password';
    ShowPassIcon = View;
  }
  return (
    <Box>
      <Box>
        <Form
          onSubmit={({value}) => {
            hideAlert();
            setLoading(true);
            api
              .post('users/register', {...value})
              .then(() => {
                showAlert({message: 'Registered user!', status: 'ok'});
                history.push(loginRoute);
              })
              .catch(err => {
                showAlert({message: decodeError(err), status: 'critical'});
                setLoading(false);
              });
          }}
        >
          <FormField
            name="email"
            label="Email"
            validate={{
              regexp: /^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/,
              message: 'Invalid format',
            }}
            required
          >
            <TextInput name="email" />
          </FormField>
          <FormField name="username" label="Username" required>
            <TextInput name="username" />
          </FormField>
          <FormField name="password" label="Password" required>
            <Box direction="row" align="center">
              <TextInput name="password" type={passType} plain />
              <Button
                icon={<ShowPassIcon size="medium" />}
                onClick={() => setShowPass(!showPass)}
                focusIndicator={false}
              />
            </Box>
          </FormField>
          <LoadingButton
            margin={{top: 'small'}}
            type="submit"
            primary
            label="Sign up"
            fill="horizontal"
            icon={<RegisterIcon />}
            loading={loading}
          />
        </Form>
      </Box>
      <Box margin={{top: 'small'}} align="center">
        <Text>
          Already have an account?&nbsp;
          <Anchor label="Sign in" onClick={() => history.push(loginRoute)} />
        </Text>
      </Box>
    </Box>
  );
};
Register.propTypes = {
  showAlert: PropTypes.func.isRequired,
  hideAlert: PropTypes.func.isRequired,
};
Register.mapActionsToProps = {
  showAlert: showAlertAct,
  hideAlert: hideAlertAct,
};
export default connect(null, Register.mapActionsToProps)(Register);
