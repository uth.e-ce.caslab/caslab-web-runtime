import PropTypes from 'prop-types';
import React from 'react';
import {connect} from 'react-redux';
import {Redirect, Route} from 'react-router-dom';
import {loginRoute} from './config';

function PrivateRoute({children, authenticated, ...rest}) {
  return (
    <Route
      {...rest}
      render={({location}) =>
        authenticated ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: loginRoute,
              state: {from: location},
            }}
          />
        )
      }
    />
  );
}
PrivateRoute.propTypes = {
  authenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  authenticated: state.auth.authenticated,
});

export default connect(mapStateToProps)(PrivateRoute);
