import {useContext, useEffect, useState} from 'react';
import {
  attachToProject,
  detachFromProject,
  WebSocketContext,
} from './WebSocket';

const useProjectFeed = projectId => {
  const feeds = useContext(WebSocketContext);
  const [attachAttempt, setAttachAttempt] = useState(0);
  useEffect(() => {
    const usedId = projectId; // keep copy for correct detach operation
    const attachProm = attachToProject(feeds, usedId)
      .then(() => true)
      .catch(() => {
        setAttachAttempt(attachAttempt + 1); // retrigger hook
        return false;
      });
    return () => {
      attachProm.then(attached => {
        if (attached) return detachFromProject(feeds, usedId);
      });
    };
  }, [attachAttempt, feeds, projectId]);
};

export default useProjectFeed;
