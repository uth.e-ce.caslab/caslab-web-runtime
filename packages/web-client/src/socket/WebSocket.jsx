import React, {createContext, useEffect, useState} from 'react';
import {io} from 'socket.io-client';
import {useSelector} from 'react-redux';

const WebSocketContext = createContext(null);
export {WebSocketContext};

function connect(token) {
  const projects = io('/projects', {query: `token=${token}`}); // TODO: take path from common lib
  const feeds = {projects};
  projects.attachTo = ''; // placeHolder for re attaching to project on re connection;
  let connectCounter = 0;
  projects.on('connect', async () => {
    connectCounter++;
    if (connectCounter === 1) {
      console.log('WebSocket', 'Connected');
      return;
    }
    // when reconnecting we can check for attaching to project again
    console.log('WebSocket', 'Reconnected');
    if (projects.attachTo) await attachToProject(feeds, projects.attachTo);
  });
  return feeds;
}

function disconnect(feeds) {
  if (feeds) {
    feeds.projects.disconnect();
  }
}

export async function attachToProject(feeds, projectId) {
  return new Promise((resolve, reject) => {
    if (feeds) {
      feeds.projects.attachTo = projectId;
      feeds.projects.emit('attachToProject', projectId, error => {
        if (error) {
          console.error(
            'WebSocket',
            `Failed to attach to project=${projectId} feed`,
            error,
          );
          reject(error);
          return;
        }
        console.log('WebSocket', `Attached to project=${projectId} feed`);
        resolve();
      });
    } else {
      reject(new Error('Web socket uninitialized'));
    }
  });
}

export async function detachFromProject(feeds, projectId) {
  return new Promise((resolve, reject) => {
    if (feeds) {
      feeds.projects.attachTo = ''; // clear attachment
      feeds.projects.emit('detachFromProject', projectId, error => {
        if (error) {
          console.error(
            'WebSocket',
            `Failed to detach from project=${projectId} feed`,
            error,
          );
        } else {
          console.info('WebSocket', `Detach from project=${projectId} feed`);
        }
        resolve();
      });
    } else {
      reject(new Error('Web socket uninitialized'));
    }
  });
}

export default ({children}) => {
  const [feeds, setFeeds] = useState(null);
  const token = useSelector(state => state.auth.token);
  useEffect(() => {
    const newFeeds = connect(token);
    setFeeds(newFeeds);
    return () => {
      disconnect(newFeeds);
    };
  }, [token]);
  return (
    <WebSocketContext.Provider value={feeds}>
      {children}
    </WebSocketContext.Provider>
  );
};
