import {ConnectedRouter} from 'connected-react-router';
import {Grommet} from 'grommet';
import React from 'react';
import {Provider} from 'react-redux';
import {Redirect, Switch} from 'react-router-dom';
import {Auth, authRoute, PrivateRoute, PublicOnlyRoute} from './auth';
import {Dashboard} from './dashboard';
import configureStore, {history} from './store/configureStore';
import './App.css';
import WebSocket from './socket/WebSocket';
import {AlertsManager} from './alerts';

const theme = {
  global: {
    colors: {
      brand: '#00458E',
      focus: 'brand',
    },
    font: {
      family: 'Roboto',
    },
  },
};

export const store = configureStore();

function App() {
  return (
    <Provider store={store}>
      <AlertsManager />
      <Grommet theme={theme} full>
        <ConnectedRouter history={history}>
          <Switch>
            <PublicOnlyRoute path={authRoute}>
              <Auth />
            </PublicOnlyRoute>
            <PrivateRoute path="/">
              <WebSocket>
                <Dashboard />
              </WebSocket>
            </PrivateRoute>
            <Redirect to="/" />
          </Switch>
        </ConnectedRouter>
      </Grommet>
    </Provider>
  );
}

export default App;
