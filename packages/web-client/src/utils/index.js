import api from './api';
import {unwrapResult} from '@reduxjs/toolkit';
export {default as api} from './api';

export const bindFunctions = (self, functions) => {
  functions.forEach(f => {
    self[f] = self[f].bind(self);
  });
};

export function updateItemInArray(array, idProp, idValue, updateItemCallback) {
  return array.map(item => {
    if (item[idProp] !== idValue) return item; // do not touch rest
    return updateItemCallback(item); // transform found item
  });
}

export function strOrEmpty(value, emptyValue = '<Empty>') {
  return value || emptyValue;
}

export const asyncActionGenerator = (fn, value, showAlert, msg) => async () => {
  const action = await fn(value);
  try {
    unwrapResult(action);
    showAlert({
      message: `${msg} success`,
      status: 'ok',
    });
  } catch (unused) {
    showAlert({
      message: `${msg} failure`,
      status: 'critical',
    });
  }
};

export default {
  bindFunctions,
  updateItemInArray,
  api,
  strOrEmpty,
  asyncActionGenerator,
};
