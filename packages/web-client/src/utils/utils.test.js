import {strOrEmpty} from './index';

const {bindFunctions} = require('.');

describe('Testing utils', () => {
  describe('Testing bindFunctions', () => {
    function incrBy2() {
      return this.prop + 2;
    }
    function decrBy1() {
      return this.prop - 1;
    }
    test('bind a method', () => {
      function incrBy2() {
        return this.prop + 2;
      }
      const x = {
        incrBy2,
        prop: 1,
      };
      bindFunctions(x, ['incrBy2']);
      expect(x.incrBy2()).toBe(3);
    });
    test('bind multiple methods', () => {
      const x = {
        incrBy2,
        decrBy1,
        prop: 1,
      };
      bindFunctions(x, ['incrBy2', 'decrBy1']);
      expect(x.incrBy2()).toBe(3);
      expect(x.decrBy1()).toBe(0);
    });
  });
  describe('Testing strOrEmpty', () => {
    test('return provided string', () => {
      expect(strOrEmpty('strVal')).toBe('strVal');
    });
    test('return empty value', () => {
      expect(strOrEmpty('')).toBe('<Empty>');
      expect(strOrEmpty(undefined)).toBe('<Empty>');
      expect(strOrEmpty(null)).toBe('<Empty>');
    });
  });
});
