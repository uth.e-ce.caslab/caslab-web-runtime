import axios from 'axios';

export const BASE_URL = '/api';

export const instance = axios.create({
  baseURL: BASE_URL,
  headers: {'Content-Type': 'application/json'},
});

export const addInterceptor = (type, normalHandler, errorHandler) => {
  if (type !== 'request' && type !== 'response') {
    throw new Error('Wrong interceptor type.');
  }
  return instance.interceptors[type].use(normalHandler, errorHandler);
};

export const setBearerToken = token => {
  instance.defaults.headers.common['Authorization'] = `Bearer ${token}`;
};

export const setAuthenticationHeader = token => {
  instance.defaults.headers.common['Authorization'] = token;
};

export const post = (url, data, config) => {
  return instance.post(url, data, config);
};

export const postFile = (url, data) => {
  const formData = new FormData();
  Object.entries(data).forEach(([key, value]) => formData.append(key, value));
  return post(url, formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
};

export const patch = (url, data) => {
  return instance.patch(url, data);
};

export const get = (url, params) => {
  return instance.get(url, {params});
};

export const getFile = (url, params) => {
  return instance
    .get(url, {params, responseType: 'blob'})
    .then(({headers, data}) => {
      const contentDisposition = headers['content-disposition'];
      const contentType = headers['content-type'];
      if (!contentDisposition) {
        throw new Error('Missing content disposition header');
      } else if (!contentType) {
        throw new Error('Missing content type');
      }
      const filename = contentDisposition.split('filename=')[1];
      const blob = new Blob([data], {type: contentType});
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.setAttribute('download', filename);
      document.body.appendChild(link);
      link.click();
      link.remove(); // cleanup
      return filename;
    });
};

export const del = url => {
  return instance.delete(url);
};

export default {
  BASE_URL,
  instance,
  addInterceptor,
  setBearerToken,
  setAuthenticationHeader,
  post,
  postFile,
  patch,
  get,
  getFile,
  del,
};
