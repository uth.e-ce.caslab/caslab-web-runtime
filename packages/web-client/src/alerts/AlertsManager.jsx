import PropTypes from 'prop-types';
import React from 'react';
import {connect} from 'react-redux';
import Alert from './Alert';
import {hideAlert as hideAlertAct} from '../store/state/alerts';

function AlertsManager({alerts, hideAlert}) {
  if (alerts.isAlertHidden) return null; // nothing to display here
  return (
    <Alert
      status={alerts.alertStatus}
      message={alerts.alertMessage}
      hide={hideAlert}
    />
  );
}

AlertsManager.propTypes = {
  alerts: PropTypes.shape({
    isAlertHidden: PropTypes.bool.isRequired,
    alertMessage: PropTypes.string.isRequired,
    alertStatus: PropTypes.string.isRequired,
  }).isRequired,
  hideAlert: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  alerts: state.alerts,
});

const mapActionsToProps = {
  hideAlert: hideAlertAct,
};

export default connect(mapStateToProps, mapActionsToProps)(AlertsManager);
