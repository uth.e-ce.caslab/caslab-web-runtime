'use strict';

const path = require('path');
const root = path.resolve(__dirname, '../../../web-client/build');

/**
 * Defines a middleware that serve's the client UI at all requests that reach this middleware.
 *
 * See: [Loopback Middleware](https://docs.strongloop.com/display/public/LB/Defining+middleware)
 */
module.exports = () => (req, res) => {
  res.sendFile('/index.html', {root});
};
