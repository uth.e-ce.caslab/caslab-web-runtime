'use strict';

const loopback = require('loopback');
const boot = require('loopback-boot');

global.Promise = require('bluebird');
global.logger = console; // TODO: add better logger later on.

const app = (module.exports = loopback());

app.start = async () => {
  await Promise.fromCallback(cb => app.set('server', app.listen(cb)));
  app.emit('started');
  const baseUrl = app.get('url').replace(/\/$/, '');
  logger.info('Web server listening at: %s', baseUrl);
  if (app.get('loopback-component-explorer')) {
    const explorerPath = app.get('loopback-component-explorer').mountPath;
    logger.info('Browse your REST API at %s%s', baseUrl, explorerPath);
  }
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) app.start();
});
