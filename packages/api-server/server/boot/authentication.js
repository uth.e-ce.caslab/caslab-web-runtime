'use strict';

/**
 * Enables application's authentication system
 *
 * @param {loopback} app
 * @returns {Promise<void>}
 */
module.exports = app => {
  app.enableAuth();
};
