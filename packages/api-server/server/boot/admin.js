'use strict';

/**
 * Initializes admin role, registers admin role resolver that decides whether login user is admin or
 * not and finally checks the existence of the default admin user and if missing it creates the
 * default admin user.
 *
 * @param {loopback} app
 * @returns {Promise<User>}
 */
module.exports = app => {
  const {Role, AppUser} = app.models;
  const ADMIN_ROLE = 'admin';
  const defaultAdmin = {
    username: 'admin',
    email: 'admin@caslab.com',
    password: 'admin1234', // TODO: retrieve from env later on...
    roles: [ADMIN_ROLE],
    emailVerified: true, // skip email verification
    active: true, // skip user activation
  };
  Role.registerResolver(ADMIN_ROLE, async (role, context) => {
    const {userId} = context.accessToken || {};
    if (!userId) return false; // Unauth request
    // auth request, check if user exists
    const user = await AppUser.findById(userId, {
      fields: {id: true, roles: true},
    });
    if (!user) {
      const err = new Error(`Missing user with id='${userId}'`);
      err.statusCode = 400;
      err.status = err.statusCode;
      throw err;
    }
    // found user, check if its roles contain the admin role
    return user.roles.includes(ADMIN_ROLE);
  });
  return AppUser.findOrCreate(
    {where: {username: defaultAdmin.username}},
    defaultAdmin,
  )
    .then(() =>
      logger.info(
        {type: 'ADMIN_INIT_SUCCESS'},
        'Admin initialization completed',
      ),
    )
    .catch(err =>
      logger.error(
        {type: 'ADMIN_INIT_FAILURE', err},
        'Error during admin initialization',
      ),
    );
};
