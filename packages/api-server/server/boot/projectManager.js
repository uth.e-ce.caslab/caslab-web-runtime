'use strict';

const ProjectManager = require('../lib/ProjectManager');

/**
 * Initializes the project manager.
 *
 * @param {loopback} app
 */
module.exports = app => {
  const projectManager = new ProjectManager(app);
  app.set('projectManager', projectManager);
  app.once('started', async () => {
    try {
      await projectManager.start();
    } catch (err) {
      logger.error(
        {type: 'PROJECT_MANAGER_START_FAILURE', err},
        'Error while starting project manager',
      );
      setTimeout(process.exit, 2000, 1); // give some time and then exit
    }
  });
};
