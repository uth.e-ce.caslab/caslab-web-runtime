'use strict';

const assert = require('assert');
const SocketServer = require('../lib/SocketServer');

/**
 * Initializes socket.io server.
 *
 * @param {loopback} app
 */
module.exports = app => {
  const socketServer = new SocketServer(app);
  app.set('socketServer', socketServer);
  app.once('started', async () => {
    try {
      await socketServer.start();
    } catch (err) {
      logger.error(
        {type: 'SOCKET_SERVER_START_FAILURE', err},
        'Error while starting socket server',
      );
      setTimeout(process.exit, 2000, 1); // give some time and then exit
    }
  });
};
