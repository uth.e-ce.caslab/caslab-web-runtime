'use strict';

const assert = require('assert');
const util = require('util');
const {execFile, execFileSync} = require('child_process');
const {spawn} = require('node-pty');

const execFileAsync = util.promisify(execFile);

/**
 * @typedef {IPty} ProjectProcess
 * @property {string} projectId
 */

/**
 * Project manager functionality that will handle project processes. Its main operation is to spawn a process when
 * a project starts and stops it when it stops.
 */
class ProjectManager {
  /**
   * Creates project manager instance
   * @param {loopback} app
   */
  constructor(app) {
    assert(app, 'Missing app');

    /**
     * Server instance
     * @type {loopback}
     */
    this.app = app;

    /**
     * Socket server instance. Will be populated at start
     * @type {?SocketServer}
     */
    this.socketServer = null;

    /**
     * Holds all started projects. Uses project's id as key and project's process id as value.
     * Serves as LUP using the projectId as key.
     * @type {Map<string, ProjectProcess>}
     */
    this.runningProjects = new Map();

    /**
     * Holds all started processes for the projects. Uses project's process id as key and project's id as value.
     * Serves as LUP using the project's pid.
     * @type {Map<string, ProjectProcess>}
     */
    this.processes = new Map();
  }

  /**
   * Starts the project manager
   * @returns {Promise<void>}
   */
  async start() {
    this.socketServer = this.app.get('socketServer');
    assert(this.socketServer, 'Socket server not initialized');
    const {Project} = this.app.models;
    const {count} = await Project.updateAll(
      {state: {neq: 'IDLE'}},
      {$set: {state: 'IDLE', stoppedAt: new Date()}},
    );
    if (count) {
      logger.warn(
        {type: 'STOPPED_RUNNING_PROJECTS', count},
        'Stopped some running projects',
      );
    }
    logger.info({type: 'PROJECT_MANAGER_STARTED'}, 'Started project manager');
  }

  /**
   * Starts the project. If there is no process running for this project it starts it, otherwise it does nothing.
   * @param {string} projectId
   * @returns {Promise<void>}
   */
  async startProject(projectId) {
    assert(projectId, 'Missing projectId');
    const {
      runningProjects,
      processes,
      app: {
        models: {Project},
      },
    } = this;
    const desiredState = 'RUNNING';
    await Project.updateAll(
      {id: projectId, state: {neq: desiredState}},
      {$set: {state: desiredState, startedAt: new Date()}},
    );
    if (runningProjects.has(projectId)) {
      logger.warn(
        {type: 'START_PROJECT_UNNEEDED', projectId},
        'Tried to start running project',
      );
      return; // process for this project already exists, so do nothing
    }
    const process = await this._startProcess(projectId);
    process.projectId = projectId;
    const projectProcess = process;
    runningProjects.set(projectId, projectProcess);
    processes.set(process.pid, projectProcess);
    logger.info(
      {type: 'STARTED_PROJECT', projectId},
      'Started project successfully',
    );
    await this.socketServer.projectStateChange(projectId, desiredState);
  }

  /**
   * Stops the project
   * @param {string} projectId
   * @returns {Promise<void>}
   */
  async stopProject(projectId) {
    assert(projectId, 'Missing projectId');
    const {
      runningProjects,
      processes,
      app: {
        models: {Project},
      },
    } = this;
    const desiredState = 'IDLE';
    await Project.updateAll(
      {id: projectId, state: {neq: desiredState}},
      {$set: {state: desiredState, stoppedAt: new Date()}},
    );
    if (!runningProjects.has(projectId)) {
      logger.warn(
        {type: 'STOP_PROJECT_UNNEEDED', projectId},
        'Tried to stop idle project',
      );
      return; // no process for this project, so do nothing
    }
    const process = runningProjects.get(projectId);
    await this._stopProcess(projectId, process);
    runningProjects.delete(projectId);
    processes.delete(process.id);
    logger.info(
      {type: 'STOPPED_PROJECT', projectId},
      'Stopped project successfully',
    );
    await this.socketServer.projectStateChange(projectId, desiredState);
  }

  /**
   * Retrieves file/directory from a running project's running container.
   * @param {string} projectId
   * @param {string} path - The target file/directory path.
   * @returns {Promise<Buffer>}
   */
  async getFile(projectId, path) {
    assert(projectId, 'Missing projectId');
    assert(path && typeof path === 'string', 'Missing or Invalid path');
    logger.debug(
      {type: 'GET_FILE_STARTED', projectId, path},
      'Started retrieving file from project container',
    );
    const {stdout: fileData} = await execFileAsync(
      'docker',
      [
        'exec',
        ProjectManager.createContainerName(projectId),
        'tar',
        '-czf',
        '-',
        path,
      ],
      {encoding: 'buffer', maxBuffer: ProjectManager.GET_FILE_MAX_SIZE},
    );
    logger.debug(
      {type: 'GET_FILE_FINISHED', projectId, path, size: fileData.length},
      'Retrieved file from project container',
    );
    return fileData;
  }

  /**
   * Stores file to a running project's running container.
   * @param {string} projectId
   * @param {string} path - The path that will be used to store the file data.
   * @param {Buffer} data
   */
  async storeFile(projectId, path, data) {
    assert(projectId, 'Missing projectId');
    assert(path && typeof path === 'string', 'Missing or Invalid path');
    assert(Buffer.isBuffer(data), 'Missing file');
    logger.debug(
      {type: 'STORE_FILE_STARTED', projectId, path, size: data.length},
      'Started storing file to project container',
    );
    // TODO: replace with an async exec operation for better performance
    execFileSync(
      'docker',
      [
        'exec',
        '-i',
        ProjectManager.createContainerName(projectId),
        'sh',
        '-c',
        `cat - > ${path}`,
      ],
      {input: data},
    );
    logger.debug(
      {type: 'STORE_FILE_FINISHED', projectId, path},
      'Stored file to project container',
    );
  }

  /**
   * Writes shell input into the corresponding project's process
   * @param {string} projectId
   * @param {string} data
   */
  shellInput(projectId, data) {
    assert(projectId, 'Missing projectId');
    const process = this.runningProjects.get(projectId) || {};
    assert(process, 'Process not found');
    process.write(data);
  }

  /**
   * Starts a new project process.
   * @private
   * @param {string} projectId
   * @returns {IPty} Returns the created process
   */
  async _startProcess(projectId) {
    assert(projectId, 'Missing projectId');
    const {socketServer} = this;
    const name = ProjectManager.createContainerName(projectId);
    // just for failsafe, try to kill a previous container with the same tag
    await execFileAsync('docker', ['kill', name]).catch(() => {});
    const process = spawn(
      'docker',
      [
        'run',
        '--name',
        name,
        '--rm',
        '-ti',
        ProjectManager.PROJECT_IMAGE,
        '/bin/bash',
      ],
      {
        name: projectId,
      },
    );
    process.onExit(this.stopProject.bind(this, projectId));
    process.onData(
      socketServer.projectShellOutput.bind(socketServer, projectId),
    );
    logger.debug(
      {type: 'PROCESS_STARTED', pid: process.pid, projectId},
      'Started project process',
    );
    return process;
  }

  /**
   * Stops the projects process.
   * @private
   * @param {string} projectId
   * @param {ProjectProcess} process
   * @returns {Promise<string>}
   */
  async _stopProcess(projectId, process) {
    assert(projectId, 'Missing projectId');
    assert(process, 'Missing process');
    process.removeAllListeners('data');
    process.removeAllListeners('exit');
    process.kill();
    logger.debug(
      {type: 'PROCESS_STOPPED', pid: process.pid, projectId},
      'Stopped project process',
    );
  }

  /**
   * Creates a docker container name for the provided project.
   * @param {string} projectId
   * @returns {string}
   * @static
   */
  static createContainerName(projectId) {
    return `caslab-project-${projectId}`;
  }
}

/**
 * The docker image that will be used by project processes.
 * @type {string}
 */
ProjectManager.PROJECT_IMAGE = process.env.PROJECT_IMAGE || 'ubuntu';

/**
 * The maximum allowed project getFile operation file size.
 * @type {number}
 * @default 100 MB
 */
ProjectManager.GET_FILE_MAX_SIZE =
  process.env.PROJECT_GET_FILE_MAX_SIZE || 100 * 1024 * 1024; // 100 MB

module.exports = ProjectManager;
