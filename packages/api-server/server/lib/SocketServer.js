'use strict';

const assert = require('assert');
const newSocketIO = require('socket.io');

// TODO: move static names like 'shellInput' topic into common library

/**
 * Socket.io server functionality
 */
class SocketServer {
  /**
   * Creates a new socket server instance
   * @param {loopback} app
   */
  constructor(app) {
    assert(app, 'Missing app');

    /**
     * Server instance
     * @type {loopback}
     */
    this.app = app;

    /**
     * Socket.io instance. Will be populated when server starts
     * @type {?socket.io.Server}
     */
    this.io = null;

    /**
     * Socket.io instance. Will be populated when server starts
     * @type {?ProjectManager}
     */
    this.projectManager = null;

    /**
     * The projects namespace
     * @type {?socket.io.Namespace}
     */
    this.projectsNamespace = null;
  }
  async start() {
    const server = this.app.get('server');
    assert(server, 'Server not initialized');
    this.projectManager = this.app.get('projectManager');
    assert(this.projectManager, 'Project manager not initialized');
    this.io = newSocketIO(server);
    this.enableProjectsNamespace();
    logger.info({type: 'SOCKET_SERVER_STARTED'}, 'Started socket server');
  }

  /**
   * Checks whether socket connection is allowed or not.
   * @param {socket.io.Socket} socket
   * @param {function} next
   */
  async authorizeMiddleware(socket, next) {
    const token = socket?.handshake?.query?.token;
    if (!token) {
      next(new Error('Missing token'));
      return;
    }
    let decodedToken;
    try {
      decodedToken = await this.app.models.AppAccessToken.findById(token);
    } catch (err) {
      logger.error(
        {type: 'CHECK_TOKEN_ERROR_OCCURRED', err},
        'Error during token check',
      );
    }
    if (!decodedToken) {
      next(new Error('Invalid token'));
      return;
    }
    socket.decodedToken = decodedToken;
    next();
  }

  /**
   * Enables project socket namespace and paths.
   */
  enableProjectsNamespace() {
    this.projectsNamespace = this.io
      .of(SocketServer.PROJECTS_PATH)
      .use(this.authorizeMiddleware.bind(this))
      .on('connection', this.projectsConnection.bind(this));
  }

  /**
   * Attaches projects namespace listeners to the socket
   * @param {socket.io.Socket} socket
   */
  projectsConnection(socket) {
    assert(socket, 'Missing socket');
    const userId = SocketServer.socketToUserId(socket);
    logger.info(
      {type: 'PROJECTS_SOCKET_CONNECTED', userId},
      'Client connected to projects namespace',
    );
    socket
      .on('attachToProject', this.attachToProject.bind(this, socket))
      .on('detachFromProject', this.detachFromProject.bind(this, socket))
      .on('shellInput', this.projectShellInput.bind(this, socket))
      .on('disconnect', this.projectsDisconnect.bind(this, socket))
      .on('error', this.projectsError.bind(this, socket));
  }

  /**
   * Attaches the socket to the project's feed.
   * @param {socket.io.Socket} socket
   * @param {string} projectId
   * @param {function} confirm
   * @returns {Promise<void>}
   */
  async attachToProject(socket, projectId, confirm) {
    let userId;
    try {
      assert(socket, 'Missing socket');
      assert(projectId, 'Missing projectId');
      assert(!confirm || typeof confirm === 'function', 'Invalid confirm');
      userId = SocketServer.socketToUserId(socket);
    } catch (err) {
      logger.error(
        {
          type: 'ATTACH_TO_PROJECT_INPUT_ERROR',
          socketId: socket.id,
          projectId,
          confirm,
        },
        'Attach to project input error',
      );
      if (confirm && typeof confirm === 'function') {
        confirm({message: 'Invalid Input'});
      }
      return;
    }
    try {
      await this.checkProjectOwnership(userId, projectId);
      await socket.join(projectId);
      logger.info(
        {type: 'ATTACHED_TO_PROJECT', projectId, userId, socketId: socket.id},
        'User attached to project feed',
      );
      if (confirm) confirm();
    } catch (err) {
      logger.error(
        {
          type: 'ATTACH_TO_PROJECT_FAILED',
          projectId,
          userId,
          socketId: socket.id,
          err,
        },
        'Attach to project feed failed',
      );
      if (confirm) confirm({message: 'Unauthorized Project'});
    }
  }

  /**
   * Checks whether user owns the project or not. Returns the project in case of success for further inspection.
   * Throws error if not.
   * @param {string} userId
   * @param {string} projectId
   * @returns {Promise<Project>}
   */
  async checkProjectOwnership(userId, projectId) {
    return this.app.models.Project.findOne({
      where: {id: projectId, userId},
      fields: {id: true, state: true},
    });
  }

  /**
   * Detaches the socket from the project's feed.
   * @param {socket.io.Socket} socket
   * @param {string} projectId
   * @param {function} [confirm]
   * @returns {Promise<void>}
   */
  async detachFromProject(socket, projectId, confirm) {
    try {
      assert(socket, 'Missing socket');
      assert(projectId, 'Missing projectId');
      assert(!confirm || typeof confirm === 'function', 'Invalid confirm');
    } catch (err) {
      logger.error(
        {
          type: 'DETACH_FROM_PROJECT_INPUT_ERROR',
          socketId: socket.id,
          projectId,
          confirm,
        },
        'Detach from project input error',
      );
      if (confirm && typeof confirm === 'function') {
        confirm({message: 'Invalid Input'});
      }
      return;
    }
    try {
      await socket.leave(projectId);
      logger.info(
        {type: 'DETACHED_FROM_PROJECT', projectId, socketId: socket.id},
        'User detached from project feed',
      );
      if (confirm) confirm();
    } catch (err) {
      logger.error(
        {
          type: 'DETACH_FROM_PROJECT_FAILED',
          projectId,
          socketId: socket.id,
          err,
        },
        'Detach from project feed failed',
      );
      if (confirm) confirm({message: 'Detach failure'});
    }
  }

  /**
   * Attaches the socket to the project's feed.
   * @param {socket.io.Socket} socket
   * @param {string} projectId
   * @param {string} data
   * @returns {Promise<void>}
   */
  async projectShellInput(socket, projectId, data) {
    let userId;
    try {
      assert(socket, 'Missing socket');
      assert(projectId, 'Missing projectId');
      assert(data && typeof data === 'string', 'Invalid data');
      userId = SocketServer.socketToUserId(socket);
    } catch (err) {
      logger.error(
        {
          type: 'PROJECT_SHELL_INPUT_INPUT_ERROR',
          socketId: socket.id,
          projectId,
          data,
        },
        'Project-shell-input input error',
      );
      return;
    }
    try {
      const project = await this.checkProjectOwnership(userId, projectId);
      assert(project.state === 'RUNNING', 'Project is not running');
      this.projectManager.shellInput(projectId, data);
    } catch (err) {
      logger.error(
        {
          type: 'HANDLE_PROJECT_SHELL_FAILED',
          projectId,
          userId,
          socketId: socket.id,
          err,
          data,
        },
        'Project shell input handling failed',
      );
    }
  }

  /**
   * Forwards project's shell output to the clients
   * @param {string} projectId
   * @param {string} data
   * @returns {Promise<void>}
   */
  async projectShellOutput(projectId, data) {
    assert(projectId, 'Missing projectId');
    assert(data && typeof data === 'string', 'Invalid data');
    assert(this.projectsNamespace, 'Uninitialized projects namespace');
    this.projectsNamespace.to(projectId).emit('shellOutput', data);
  }

  /**
   * Forwards project's new state to the clients
   * @param {string} projectId
   * @param {string} state
   * @returns {Promise<void>}
   */
  async projectStateChange(projectId, state) {
    assert(projectId, 'Missing projectId');
    assert(state && typeof state === 'string', 'Invalid state');
    assert(this.projectsNamespace, 'Uninitialized projects namespace');
    this.projectsNamespace.to(projectId).emit('state', state);
  }

  /**
   * Handles projects namespace socket disconnect.
   * @param {socket.io.Socket} socket
   * @param {string} reason
   */
  projectsDisconnect(socket, reason) {
    assert(socket, 'Missing socket');
    const userId = SocketServer.socketToUserId(socket);
    logger.info(
      {type: 'PROJECTS_SOCKET_DISCONNECT', userId, reason, socketId: socket.id},
      'Client disconnected from projects namespace',
    );
  }

  /**
   * Handles projects namespace socket error.
   * @param {socket.io.Socket} socket
   * @param err
   */
  projectsError(socket, err) {
    const userId = SocketServer.socketToUserId(socket);
    logger.error(
      {type: 'PROJECTS_SOCKET_ERROR', err, userId, socketId: socket.id},
      'Projects socket error occurred',
    );
  }

  /**
   * Retrieves userId from socket.
   * @param {socket.io.Socket} socket
   * @returns {string}
   */
  static socketToUserId(socket) {
    assert(socket, 'Missing socket');
    const {decodedToken: {userId} = {}} = socket;
    assert(userId, 'Missing userId');
    return userId;
  }
}

/**
 * Projects namespace path.
 * @type {string}
 */
SocketServer.PROJECTS_PATH = '/projects';

module.exports = SocketServer;
