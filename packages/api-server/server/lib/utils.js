'use strict';

const assert = require('assert');

/**
 * Disables the remote methods of the provided model. It can use the provided list, based on the 'useAsWhitelist' flag either
 * as blacklist and use it to remove all those methods or as a useAsWhitelist and keep only those methods and remove the rest.
 *
 * @param {Model} Model - The target model
 * @param {string[]} [list] - The list of methods
 * @param {boolean} [useAsWhitelist = false] - Operation mode
 */
function disableRemoteMethods(Model, list = [], useAsWhitelist = false) {
  assert(Model, 'Invalid model');
  assert(Array.isArray(list), 'Invalid List');
  if (useAsWhitelist) {
    Model.sharedClass?.methods().forEach(method => {
      const fnName = method.isStatic ? method.name : `prototype.${method.name}`;
      if (list.includes(fnName)) return; // skip method from list
      Model.disableRemoteMethodByName(fnName);
    });
  } else {
    list.forEach(methodName => Model.disableRemoteMethodByName(methodName));
  }
}

/**
 * An an error that can properly pass information via http response.
 */
class HttpError extends Error {
  /**
   * Creates an HTTP error.
   * @param [message='Unspecified password']
   * @param [code='UNSPECIFIED_ERROR']
   * @param [statusCode=500]
   */
  constructor(
    message = 'Unspecified password',
    code = 'UNKNOWN_ERROR',
    statusCode = 500,
  ) {
    super(message);
    this.code = code;
    this.statusCode = statusCode;
  }
}

module.exports = {
  disableRemoteMethods,
  HttpError,
};
