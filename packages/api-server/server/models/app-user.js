'use strict';

const assert = require('assert');
const path = require('path');
const {HttpError} = require('../lib/utils');
const {disableRemoteMethods} = require('../lib/utils');
const app = require('../server');

module.exports = function (AppUser) {
  AppUser.settings.verifyOptions = {
    protocol: app.get('EMAIL_VERIFY_PROTOCOL'),
    host: app.get('EMAIL_VERIFY_HOST'),
    port: app.get('EMAIL_VERIFY_PORT'),
    type: 'email',
    from: app.get('SMTP_SOURCE'),
    subject: 'Thanks for registering.',
    template: path.resolve(__dirname, '../views/verify.ejs'),
    redirect: '/auth/verified',
  };

  disableRemoteMethods(AppUser, [
    'create',
    'upsert',
    'upsertWithWhere',
    'updateAll',
    'prototype.updateAttributes',
    'prototype.verify',
    'replaceOrCreate',
    'replaceById',
    'findOne',
    'count',
    'exists',
    'resetPassword',
    'changePassword',
    'setPassword',
    'createChangeStream',
    'prototype.__count__accessTokens',
    'prototype.__create__accessTokens',
    'prototype.__delete__accessTokens',
    'prototype.__destroyById__accessTokens',
    'prototype.__findById__accessTokens',
    'prototype.__get__accessTokens',
    'prototype.__updateById__accessTokens',
    'prototype.__count__projects',
    'prototype.__create__projects',
    'prototype.__delete__projects',
    'prototype.__destroyById__projects',
    'prototype.__findById__projects',
    'prototype.__get__projects',
    'prototype.__updateById__projects',
  ]);

  /**
   * User activation check before login.
   */
  AppUser.beforeRemote('login', async ctx => {
    const {username, email} = ctx.args.credentials;
    let query;
    if (username) {
      query = {username};
    } else if (email) {
      query = {email};
    }
    if (!query) {
      return; // let the remote hook answer to missing query params
    }
    const userToLogin = await AppUser.findOne({
      where: query,
      fields: {active: true},
    });
    if (!userToLogin) {
      return; // let the remote hook answer to no login
    }
    if (!userToLogin.active) {
      logger.warn(
        {type: 'LOGIN_DEACTIVATED_USER', query},
        'Tried to login using deactivated user credentials.',
      );
      throw new HttpError(
        'Your account is deactivated. Please contact the administration.',
        'DEACTIVATED_USER',
        401,
      );
    }
  });

  /**
   * Registers a new user.
   *
   * @param {string} username
   * @param {string} email
   * @param {string} password
   * @returns {Promise<User>}
   */
  AppUser.register = async function (username, email, password) {
    assert(username, 'Invalid username');
    assert(email, 'Invalid email');
    assert(password, 'Invalid password');
    const user = await AppUser.create({
      username,
      password,
      email,
      emailVerified: false, // user will need verification
    });
    logger.info(
      {type: 'USER_REGISTERED', username, email},
      'Registered user successfully',
    );
    return user.verifyAgain();
  };
  AppUser.remoteMethod('register', {
    description: 'Registers a new user',
    http: {
      verb: 'post',
      status: 200,
      errorStatus: 400,
    },
    accepts: [
      {
        arg: 'username',
        type: 'string',
        description: 'The username of new user',
        http: {source: 'form'},
        required: true,
      },
      {
        arg: 'email',
        type: 'string',
        description: 'The email of new user',
        http: {source: 'form'},
        required: true,
      },
      {
        arg: 'password',
        type: 'string',
        description: 'The password of new user',
        http: {source: 'form'},
        required: true,
      },
    ],
    returns: {
      arg: 'user',
      type: 'object',
      required: true,
      root: true,
    },
  });

  AppUser.origConfirm = AppUser.confirm; // taken from original User model
  /**
   * New confirm logic that wraps the original call with error handling to always lead to
   * redirects (even when error occur).
   *
   * @param {Any} userId
   * @param {String} token The validation token
   * @param {String} redirect URL to redirect the user to once confirmed
   * @returns {Promise<User>}
   */
  AppUser.confirm = async function (userId, token, redirect) {
    try {
      await AppUser.origConfirm(userId, token, redirect);
    } catch (err) {
      logger.warn({type: 'CONFIRM_ERROR', err}, 'Confirm error occurred');
    }
  };

  /**
   * Re-triggers the user's email verification flow.
   * @returns {Promise<void>}
   */
  AppUser.prototype.verifyAgain = async function () {
    const user = this;
    const {email} = user;
    assert(email, 'Missing email');
    try {
      const [response] = await Promise.all([
        user.verify({
          ...AppUser.getVerifyOptions(),
          to: email,
          user,
        }),
        user.updateAttributes({emailVerified: false}),
      ]);
      logger.info(
        {type: 'USER_VERIFY_SUCCESS', email, response},
        'Verified user successfully',
      );
    } catch (err) {
      logger.error(
        {type: 'USER_VERIFY_FAILURE', email, err},
        'Failed to send verification email',
      );
    }
  };
  AppUser.remoteMethod('prototype.verifyAgain', {
    description: "Re-triggers user's identity verification",
    http: {verb: 'post'},
  });

  /**
   * Setter for the user's active flag.
   * @param {boolean} active
   * @returns {Promise<void>}
   */
  AppUser.prototype.setActive = async function (active) {
    const {id} = this;
    try {
      await AppUser.updateAll({id}, {$set: {active}});
      logger.info(
        {type: 'USER_SET_ACTIVE_SUCCESS', active},
        'Set user active successfully',
      );
    } catch (err) {
      logger.error(
        {type: 'USER_SET_ACTIVE_FAILURE', active},
        'Failed to set user active',
      );
    }
  };
  AppUser.remoteMethod('prototype.setActive', {
    description: 'Set the active flag of the user.',
    http: {verb: 'post', path: '/setActive', status: 204, errorStatus: 400},
    accepts: [
      {
        arg: 'active',
        type: 'boolean',
        description: 'The active flag value to save.',
        http: {source: 'form'},
        required: true,
      },
    ],
  });
};
