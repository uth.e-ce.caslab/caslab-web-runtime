'use strict';

module.exports = function (AppAccessToken) {
  // TODO: add clear expired tokens flow

  /**
   * Retrieves the user roles using the userId taken from the instance data and adds it to the instance
   * @param {object} ctx
   * @returns {Promise<void>}
   * @private
   */
  AppAccessToken._applyRoles = async (ctx = {}) => {
    const {data} = ctx;
    const {Role, AppUser} = AppAccessToken.app.models;
    if (!data.userId) return; // nothing to do when no userId (filtered out case)
    data.roles = await Role.getRoles({
      principalType: AppUser.modelName,
      principalId: data.userId,
      accessToken: data,
    });
  };

  /* Operation Hooks */
  AppAccessToken.observe('loaded', AppAccessToken._applyRoles);
};
