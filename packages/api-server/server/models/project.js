'use strict';

const assert = require('assert');
const ProjectManager = require('../lib/ProjectManager');
const multer = require('multer');
const util = require('util');
const {resolve: pathResolve} = require('path');
const {disableRemoteMethods} = require('../lib/utils');
/**
 * @class Project
 * @extends PersistedModel
 * @property {string} title
 * @property {string} description
 * @property {state} state
 * @property {Date} createdAt
 * @property {Date} updatedAt
 * @property {Date} startedAt
 * @property {Date} stoppedAt
 */

module.exports = function (Project) {
  disableRemoteMethods(Project, [
    'upsert',
    'upsertWithWhere',
    'updateAll',
    'replaceOrCreate',
    'replaceById',
    'findOne',
    'count',
    'exists',
    'createChangeStream',
    'prototype.__get__user',
  ]);

  Project.storage = multer.memoryStorage();
  Project.upload = multer({storage: Project.storage});

  // will search request for a single file and also will parse any existing form text field
  Project.fileParser = util.promisify(Project.upload.single('file'));

  Project.VALID_STATES = ['IDLE', 'RUNNING'];
  Project.validatesInclusionOf('state', {
    in: Object.values(Project.VALID_STATES),
    message: 'Invalid state value',
  });

  /**
   * Retrieves the project statistics for the requesting user. If the requesting
   * user is admin then it retrieves statistics for all projects.
   * @param {object} token - The request access token
   * @returns {Promise<[number,number, number]>} All count, Idle count and running
   * count.
   */
  Project.stats = async token => {
    const {userId, roles} = token;
    const isAdmin = roles.includes('admin');
    const query = {};
    if (!isAdmin) {
      query.where = {userId};
    }
    const projects = await Project.find(query);
    const result = projects.reduce(
      (res, project) => {
        res.all++;
        switch (project.state) {
          case 'IDLE':
            res.idle++;
            break;
          case 'RUNNING':
            res.running++;
            break;
        }
        return res;
      },
      {
        all: 0,
        idle: 0,
        running: 0,
      },
    );
    return [result.all, result.idle, result.running];
  };
  Project.remoteMethod('stats', {
    description: 'Retrieves project stats',
    accepts: [
      {
        arg: 'token',
        type: 'object',
        description: 'The request access token.',
        http: ctx => ctx.req.accessToken,
      },
    ],
    http: {verb: 'get', path: '/stats', status: 200, errorStatus: 400},
    returns: [
      {
        arg: 'all',
        type: 'number',
        required: true,
        description: 'All projects count',
      },
      {
        arg: 'idle',
        type: 'number',
        required: true,
        description: 'Idle projects count',
      },
      {
        arg: 'running',
        type: 'number',
        required: true,
        description: 'Running projects count',
      },
    ],
  });

  /**
   * Setter for the state property
   * @param {string} state
   * @returns {Promise<void>}
   */
  Project.prototype.setState = async function (state) {
    const projectManager = Project.app.get('projectManager');
    assert(projectManager instanceof ProjectManager, 'Missing project manager');
    const project = this;
    switch (state) {
      case 'RUNNING':
        return projectManager.startProject(project.id.toString());
      case 'IDLE':
      default:
        return projectManager.stopProject(project.id.toString());
    }
  };
  Project.remoteMethod('prototype.setState', {
    remoting: true,
    description: 'Set the state of the project.',
    accepts: [
      {
        arg: 'state',
        description: 'The state value to save.',
        http: {source: 'form'},
        type: 'string',
      },
    ],
    http: {verb: 'post', path: '/setState', status: 204, errorStatus: 400},
  });

  /**
   * Downloads a file from a running project.
   * @param {string} path
   * @returns {Promise<[Buffer, string, string]>}
   */
  Project.prototype.download = async function (path) {
    const projectManager = Project.app.get('projectManager');
    assert(projectManager instanceof ProjectManager, 'Missing project manager');
    const project = this;
    const projectId = project.id.toString();
    const {state} = project;
    if (state !== 'RUNNING') {
      logger.error(
        {type: 'DOWNLOAD_CANCELLED', projectId, path, state},
        'Project is not running, download is cancelled',
      );
      throw new Error('Download cancelled');
    }
    let fileData;
    const date = new Date().toISOString();
    const contentType = 'application/gzip';
    const filename = `project_${projectId}_${date}.tar.gz`;
    const contentDisposition = `attachment; filename=${filename}`;
    try {
      fileData = await projectManager.getFile(projectId, path);
      logger.info(
        {type: 'DOWNLOAD_SUCCESS', projectId, path},
        'Sending file to client',
      );
    } catch (err) {
      logger.error(
        {type: 'DOWNLOAD_FAILURE', projectId, path, err},
        'Failed to download file',
      );
      throw new Error('Download failure');
    }
    return [fileData, contentType, contentDisposition];
  };
  Project.remoteMethod('prototype.download', {
    remoting: true,
    description: 'Downloads a file from a running project.',
    http: {verb: 'get', path: '/download', status: 200, errorStatus: 400},
    accepts: [
      {
        arg: 'path',
        description: 'The path to the target file to download',
        http: {source: 'query'},
        type: 'string',
        required: true,
      },
    ],
    returns: [
      {arg: 'result', type: 'file', root: true},
      {arg: 'Content-Type', type: 'string', http: {target: 'header'}},
      {arg: 'Content-Disposition', type: 'string', http: {target: 'header'}},
    ],
  });

  /**
   * Upload a file to a running project.
   * @param {object} req - The original 'req' object. This will be used to extract the file.
   * @param {object} res - The original 'res' object. This will be used to extract the file.
   * @returns {Promise<[Buffer, string, string]>}
   */
  Project.prototype.upload = async function (req, res) {
    await Project.fileParser(req, res); // extract file and path (text field) from request
    const {
      file,
      body: {path = ''}, // path is optional so add empty string as default
    } = req;
    assert(file && file.originalname, 'Missing or Invalid file');
    assert(typeof path === 'string', 'Missing or Invalid path');
    const projectManager = Project.app.get('projectManager');
    assert(projectManager instanceof ProjectManager, 'Missing project manager');
    const project = this;
    const projectId = project.id.toString();
    const {state} = project;
    const filePath = pathResolve(path, file.originalname);
    if (state !== 'RUNNING') {
      logger.error(
        {type: 'UPLOAD_CANCELLED', projectId, filePath, state},
        'Project is not running, upload is cancelled',
      );
      throw new Error('Upload cancelled');
    }
    const {size, buffer} = file;
    try {
      await projectManager.storeFile(projectId, filePath, buffer);
      logger.info(
        {type: 'UPLOAD_SUCCESS', projectId, filePath, size},
        'Uploaded file to project',
      );
    } catch (err) {
      logger.error(
        {type: 'UPLOAD_FAILURE', projectId, filePath, size, err},
        'Failed to upload file',
      );
      throw new Error('Upload failure');
    }
  };
  Project.remoteMethod('prototype.upload', {
    remoting: true,
    description: 'Uploads file to a running project.',
    http: {verb: 'post', path: '/upload', status: 204, errorStatus: 400},
    accepts: [
      {
        arg: 'req',
        description: 'The original req property',
        http: {source: 'req'},
        type: 'object',
      },
      {
        arg: 'res',
        description: 'The original res property',
        http: {source: 'res'},
        type: 'object',
      },
    ],
  });

  /**
   * List of properties that patch attributes should not update.
   * @type {string[]}
   */
  Project.patchAttributesBlacklist = ['state', 'startedAt', 'stoppedAt'];
  /**
   * Patch attributes endpoint hook that restricts update of specific properties (e.g. State)
   * @param {{args:{data:Partial<Project>}}} ctx
   * @return {Promise<void>}
   */
  Project.patchAttributesHook = async ctx => {
    const {data} = ctx.args;
    Project.patchAttributesBlacklist.forEach(attr => {
      delete data[attr];
    });
  };
  Project.beforeRemote(
    'prototype.patchAttributes',
    Project.patchAttributesHook,
  );
};
