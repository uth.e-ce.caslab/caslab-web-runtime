'use strict';

/**
 * Mixin for assigning timestamp properties to models for keeping dates of creation and update. Here we define the
 * properties to be assigned to the model and also the operation hooks that will keep them up to date. It is structured
 * upon [Loopback Mixins].
 * (https://docs.strongloop.com/display/public/LB/Defining+mixins)
 *
 * @class Timestamp
 * @constructor
 * @extensionfor loopback.Model
 * @property {Date} createdAt The date of model instance creation
 * @property {Date} updatedAt The last date of model instance update
 */
module.exports = Model => {
  Model.defineProperty('createdAt', {type: Date, defaultFn: 'now'});
  Model.defineProperty('updatedAt', {type: Date, defaultFn: 'now'});
  Model.observe('before save', (ctx, next) => {
    // Check if we have full or partial update of model to update corresponding properties
    if (ctx.instance) {
      ctx.instance.updatedAt = new Date();
    } else if (ctx.data && ctx.data.$set) {
      ctx.data.$set.updatedAt = new Date();
    } else {
      ctx.data.updatedAt = new Date();
    }
    next();
  });
};
