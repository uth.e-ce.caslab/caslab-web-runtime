'use strict';

/**
 * The roles to ignore by default
 * @type {string[]}
 */
const DEFAULT_IGNORED_ROLES = ['admin'];

/**
 * @module api-server/mixins/restrict-access
 * This Loopback model Mixin will be used to restrict access for the specific model to specific roles. Loopback provides
 * the $owner role which finds the relation to a **User** based model, and checks the related property userId to match
 * the userId in accessToken. However this cannot be done with generic functions such as find, findOne, count, create...
 * etc where no specific instance is given to check userId on. This mixin will force into the properties for generic
 * functions the inclusion of the userId property set to the value of accessToken userId.
 *
 * Supported options are for configuring the userId property other than the default `userId` on the specific model,
 * the ignoredRoles for which the mixin will not be applied and the methods which will be allowed
 *
 * The operation can be skipped by setting the Action option `skipRestriction` to `true`
 * @param {Model} model the Loopback model where the mixin will be applied
 * @param {object} [options] Options object passed from the definition of mixin in the model
 * @param {string} [options.userProperty = userId] The user property for model. Defaults to userId
 * skipped. Admin is an example where we do not want to apply restriction
 * @param {string[]} [options.ignoredRoles = [admin]] The roles for which the functionality will be
 */
module.exports = (model, options = {}) => {
  const {
    userProperty = 'userId',
    ignoredRoles = DEFAULT_IGNORED_ROLES,
  } = options;
  const makeObj = obj =>
    typeof obj === 'string' ? JSON.parse(obj) : obj || {};
  // helper parsers for each hook
  const accessHandler = (ctx, userId) => {
    if (typeof ctx.query !== 'object') ctx.query = {}; // initialize query if needed
    const {query} = ctx;
    query.where = makeObj(query.where); // make sure where is object formatted
    query.where[userProperty] = userId; // assign to query
  };
  const beforeSaveHandler = (ctx, userId) => {
    if (ctx.instance) {
      // instance save/update
      ctx.instance[userProperty] = userId;
    } else {
      // partial update
      ctx.where = makeObj(ctx.where);
      ctx.where[userProperty] = userId; // make sure data that only user owned data updated
      ctx.data = makeObj(ctx.data);
      const {data} = ctx;
      if (data.hasOwnProperty('$set')) {
        // properly handle $set operator when it exists
        data.$set[userProperty] = userId;
      } else {
        // direct object assignment
        data[userProperty] = userId;
      }
    }
  };
  const ctxParser = {
    access: accessHandler,
    'before save': beforeSaveHandler,
  };

  /**
   * Hook constructor depending on given hook name to share common logic
   *
   * @param {string} hookName The hook to get the ctx parser from
   * @returns {Function}
   */
  const createHook = hookName =>
    /**
     * The method that will be added as hook for operation hook
     *
     * @function restrictAccessHook
     * @param {object} ctx
     * @param {object} ctx.options
     * @param {boolean} [ctx.options.skipRestriction] - Flag for skipping procedure
     * @returns {Promise<void>}
     */
    async function restrictAccessHook(ctx) {
      const {options: {skipRestriction, accessToken} = {}} = ctx;
      if (skipRestriction) return; // no restriction wanted
      if (!accessToken) return; // Not remote, just continue
      // check for ignored roles in case we need to skip, we assume that the token has loaded the
      // userRoles for the specific user
      const {roles, userId, sharedAccess: {sharedUserId} = {}} = accessToken;
      const userIdToUse = userId || sharedUserId;
      if (!roles || !Array.isArray(roles)) {
        throw new Error(
          'No user roles defined in accessToken. Cannot restrict access',
        );
      }
      const ignoreRole = ignoredRoles.some(roleToIgnore =>
        roles.includes(roleToIgnore),
      );
      if (ignoreRole) return; // No restriction needed
      // we want user restriction
      ctxParser[hookName](ctx, userIdToUse); // Apply restriction according to hook name
      // All Done!
    };

  const hooks = ['access', 'before save'];
  hooks.forEach(hookName => model.observe(hookName, createHook(hookName)));
};
