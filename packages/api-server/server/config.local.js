'use strict';

const {env} = process;

const {
  HOST,
  SMTP_HOST,
  PORT,
  SMTP_USER,
  SMTP_PASS,
  SMTP_SOURCE,
  SMTP_PORT,
  EMAIL_VERIFY_PROTOCOL,
  EMAIL_VERIFY_HOST,
} = env;

let EMAIL_VERIFY_PORT; // if not set, it will auto set to app port
if (EMAIL_VERIFY_PROTOCOL === 'http') {
  EMAIL_VERIFY_PORT = 80;
} else if (EMAIL_VERIFY_PROTOCOL === 'https') {
  EMAIL_VERIFY_PORT = 443;
}

module.exports = {
  host: HOST || 'localhost',
  port: PORT || 3001,
  SMTP_HOST: SMTP_HOST || 'smtp.mailtrap.io',
  SMTP_PORT: (SMTP_PORT && parseInt(SMTP_PORT, 10)) || 2525,
  SMTP_USER: SMTP_USER || '60523324908c61',
  SMTP_PASS: SMTP_PASS || '62f2e616bc7f2f',
  SMTP_SOURCE: SMTP_SOURCE || 'noreply@caslab.com',
  EMAIL_VERIFY_PROTOCOL,
  EMAIL_VERIFY_HOST: EMAIL_VERIFY_HOST || 'localhost',
  EMAIL_VERIFY_PORT,
};
