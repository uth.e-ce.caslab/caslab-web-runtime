# Development Environment

### Javascript
Current Version for all packages and project is *v14.5.0*.

- [Node Version Manager](https://github.com/nvm-sh/nvm#installing-and-updating)
- Monorepo management tool: `npx lerna --help`

### VSCode

Addons:

- https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint
- https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

### WebStorm
NOTE: configuration tested with version *WebStorm 2020.2 RC*.