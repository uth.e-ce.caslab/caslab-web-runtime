# caslab-web-runtime

### Getting Started

1. Install docker https://docs.docker.com/engine/install & https://docs.docker.com/engine/install/linux-postinstall.
1. Make sure your shell can execute docker: `docker --version`.
1. Install [node-version-manager](https://github.com/nvm-sh/nvm#installing-and-updating).
1. Make sure your shell can execute nvm: `nvm --version`.
1. Install NodeJS 14: `nvm i 14`
1. Navigate to project root directory and open a shell.
1. Bootstrap project:
    1. Switch to correct node version: `nvm use 14`.
    1. Make sure you are using the correct nodejs version: `node -v` -> e.g. output=`v14.15.4`.
    1. Install project's root dependencies: `npm i`.
    1. Install rest dependencies: `npm run bootstrap`.
1. Host MongoDB at port 27017: `npm run mongodb`.
1. Start the project's development environment: `npm run start`.
1. You can find the user interface at http://localhost:3000.
1. You can find the server's API at http://localhost:3001/explorer.    


### Technologies
The project's main language is Javascript.

Bootstrap:
- lerna: Project Manager - https://github.com/lerna/lerna

#### API-server

Core:
- nodejs: Programming language/runtime version 14 - https://nodejs.org/dist/latest-v14.x/docs/api/
- loopback 3x: Web Framework - https://loopback.io/doc/en/lb3/
- MongoDB: NoSQL Database

more at [./packages/api-server/package.json](./packages/api-server/package.json)

#### Web Client
Core:
- React 16: UI Framework/library 
- Grommet 2: UI elements  https://v2.grommet.io/

more at [./packages/web-client/package.json](./packages/web-client/package.json)