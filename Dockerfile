ARG NODE_VERSION=14
FROM node:${NODE_VERSION} as builder

# Create build directory and use node as owner
ARG BUILD_DIR=/build
RUN mkdir -p ${BUILD_DIR} && chown node:node ${BUILD_DIR}
USER node
WORKDIR ${BUILD_DIR}

# Install project's root packages
COPY --chown=node package*.json ./
RUN npm ci

# Install source code and build
COPY --chown=node . ./
ARG SCOPES
RUN npx lerna bootstrap ${SCOPES}
RUN npx lerna run build ${SCOPES}

# TODO: later on we could uncomment below to keep only production modules
# RUN npx lerna clean -y
# RUN npx lerna bootstrap ${SCOPES} -- --production
