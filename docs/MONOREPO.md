# Monorepo

The CASlab Web Runtime repository uses [lerna](https://lernajs.io/) to multiple packages.

Packages:

- [@caslab-web-runtime/common](../packages/common): A pure library that contains JS/TS files for code/definitions sharing between packages
- [@caslab-web-runtime/api-server](../packages/api-server): The api web server microservice that is built using nodejs and [Loopback 3](https://loopback.io/doc/en/lb3/)
- [@caslab-web-runtime/web-client](../packages/web-client): The front-end that will be used by the users to interract with CASlab Web Runtime Server. It is built using [ReactJS](https://reactjs.org/).
